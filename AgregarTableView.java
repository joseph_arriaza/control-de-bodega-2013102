package org.jarriaza.utilidad;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableColumn.CellEditEvent;


import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SelectionMode;

import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tab;
import javafx.scene.control.Button;

import javafx.geometry.Pos;


import java.util.ArrayList;

import org.jarriaza.manejadores.ManejadorDetallePrestamo;
import org.jarriaza.manejadores.ManejadorHerramienta;
import org.jarriaza.manejadores.ManejadorPrestamo;
import org.jarriaza.manejadores.ManejadorUsuario;
import org.jarriaza.manejadores.ManejadorCliente;
import org.jarriaza.beans.Herramienta;
import org.jarriaza.beans.Usuario;
import org.jarriaza.beans.Cliente;
import org.jarriaza.db.Conexion;

public class AgregarTableView implements EventHandler<Event>{
	private ManejadorHerramienta mHerramienta;
	private ManejadorDetallePrestamo mDetallePrestamo;
	private Conexion cnx;
	private Tab retornarTable;
	private Label lblErr,lblPassword,lblNombreC;
	private PasswordField pfContra,pfContra2;
	private GridPane gPanelPrincipal,gPanelEditar;
	private Button editar,aceptarEdit,eliminarHer;
	private Button editarU,aceptarEditU,eliminarU;
	private Button editarC,aceptarEditC,eliminarC;
	private Button cancel;
	private TextField txtnombreH,txtcantidadH,txtNombreC;
	private Label lblnombreH,lblcanitdadH,lblPassword2;
	private BorderPane bpPrincipal;
	private Herramienta herramientaSe;
	private Usuario usuarioSe;
	private Cliente clienteSe;
	private TableView<Herramienta> tvHerramientas;
	private TableView<Usuario> tvUsuario;
	private TableView<Cliente> tvCliente;
	private ManejadorUsuario mUsuario;
	private ManejadorCliente mCliente;
	private ManejadorPrestamo mPrestamo;
	public Tab agregarTable(String tablaAgregar,Conexion cnxo,ManejadorUsuario mUssuario,ManejadorPrestamo mPrestamos){
		mPrestamo=mPrestamos;
		cancel=new Button("Cancelar");
		mUsuario=mUssuario;
		bpPrincipal=new BorderPane();
		retornarTable=new Tab();
		lblErr=new Label();
		gPanelPrincipal=new GridPane();
		cnx=cnxo;
		if(tablaAgregar.equals("herramientas")){
			editar=new Button("Editar");
			editar.addEventHandler(ActionEvent.ACTION,this);
			eliminarHer=new Button("Elimiar");
			eliminarHer.addEventHandler(ActionEvent.ACTION,this);
			mHerramienta=new ManejadorHerramienta(cnx);
			mHerramienta.setUsuarioConectado(mUsuario.getUsuarioAutenticado());
			tvHerramientas = new TableView<Herramienta>(mHerramienta.getListaHerramientas());
			TableColumn columaNombre = new TableColumn("HERRAMIENTA");  
	    	columaNombre.setCellValueFactory(new PropertyValueFactory<Herramienta,String>("nombre"));
			columaNombre.setPrefWidth(100);
			TableColumn columnaCantidad = new TableColumn("CANTIDAD");  
	    	columnaCantidad.setCellValueFactory(new PropertyValueFactory<Herramienta,String>("cantidad"));
			columnaCantidad.setPrefWidth(100);
			tvHerramientas.getColumns().setAll(columaNombre,columnaCantidad);
			tvHerramientas.setPrefWidth(202);
			gPanelPrincipal.add(tvHerramientas,0,0,2,25);
			gPanelPrincipal.add(editar,3,0);
			gPanelPrincipal.add(eliminarHer,3,1);
			gPanelPrincipal.setVgap(8);
			gPanelPrincipal.setHgap(8);
			tvHerramientas.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			gPanelPrincipal.setAlignment(Pos.CENTER);
			bpPrincipal.setLeft(gPanelPrincipal);
			retornarTable.setContent(bpPrincipal);
			retornarTable.setText("Herramientas");
		}else if(tablaAgregar.equals("usuario")){
			editarU=new Button("Editar");
			editarU.addEventHandler(ActionEvent.ACTION,this);			
			eliminarU=new Button("Eliminar");
			eliminarU.addEventHandler(ActionEvent.ACTION,this);
			tvUsuario=new TableView<Usuario>(mUsuario.getListaUsuarios());
			
			TableColumn columaNombre = new TableColumn("NOMBRE");  
	    	columaNombre.setCellValueFactory(new PropertyValueFactory<Usuario,String>("nombre"));
			columaNombre.setPrefWidth(100);
			TableColumn columnaUsuario = new TableColumn("USERNAME");  
	    	columnaUsuario.setCellValueFactory(new PropertyValueFactory<Usuario,String>("username"));
			columnaUsuario.setPrefWidth(100);
			TableColumn columnaPassw = new TableColumn("PASSWORD");  
	    	columnaPassw.setCellValueFactory(new PropertyValueFactory<Usuario,String>("passw"));
			columnaPassw.setPrefWidth(100);
			tvUsuario.getColumns().setAll(columaNombre,columnaUsuario,columnaPassw);
			tvUsuario.setPrefWidth(302);
			gPanelPrincipal.add(tvUsuario,0,0,3,25);
			gPanelPrincipal.add(editarU,4,0);
			gPanelPrincipal.add(eliminarU,4,1);
			gPanelPrincipal.setVgap(8);
			gPanelPrincipal.setHgap(8);
			tvUsuario.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			gPanelPrincipal.setAlignment(Pos.CENTER);
			bpPrincipal.setLeft(gPanelPrincipal);
			retornarTable.setContent(bpPrincipal);
			retornarTable.setText("Usuario");
			
		}else if(tablaAgregar.equals("cliente")){
		
			mCliente=new ManejadorCliente(cnx);
			mCliente.setUsuarioConectado(mUsuario.getUsuarioAutenticado());
			editarC=new Button("Editar");
			editarC.addEventHandler(ActionEvent.ACTION,this);			
			eliminarC=new Button("Eliminar");
			eliminarC.addEventHandler(ActionEvent.ACTION,this);
			tvCliente=new TableView<Cliente>(mCliente.getlistaClientes());
			
			TableColumn columaNombre = new TableColumn("NOMBRE");  
	    	columaNombre.setCellValueFactory(new PropertyValueFactory<Cliente,String>("nombre"));
			columaNombre.setPrefWidth(100);
			tvCliente.getColumns().setAll(columaNombre);
			tvCliente.setPrefWidth(102);
			gPanelPrincipal.add(tvCliente,0,0,1,25);
			gPanelPrincipal.add(editarC,3,0);
			gPanelPrincipal.add(eliminarC,3,1);
			gPanelPrincipal.setVgap(8);
			gPanelPrincipal.setHgap(8);
			tvCliente.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
			gPanelPrincipal.setAlignment(Pos.CENTER);
			bpPrincipal.setLeft(gPanelPrincipal);
			retornarTable.setContent(bpPrincipal);
			retornarTable.setText("Usuario");
		}
		return retornarTable;
	}

	/**
		Metodo de eventos
		@param event evento que se recibe
	*/
	public void handle(Event event){
		if(event instanceof ActionEvent){
			if(event.getSource()==editar){
				herramientaSe = (Herramienta)tvHerramientas.getSelectionModel().getSelectedItem();
				if(herramientaSe!=null){
					gPanelEditar=new GridPane();
					txtnombreH=new TextField(herramientaSe.getNombre());
					txtnombreH.addEventHandler(KeyEvent.KEY_RELEASED,this);
					txtcantidadH=new TextField(""+herramientaSe.getCantidad());
					txtcantidadH.addEventHandler(KeyEvent.KEY_RELEASED,this);
					lblcanitdadH=new Label("Cantidad: ");
					lblnombreH=new Label("Nombre: ");
					aceptarEdit=new Button("Aceptar");
					aceptarEdit.addEventHandler(ActionEvent.ACTION,this);
					cancel.addEventHandler(ActionEvent.ACTION,this);
					gPanelEditar.add(lblnombreH,0,0);
					gPanelEditar.add(txtnombreH,1,0);
					gPanelEditar.add(lblcanitdadH,0,1);
					gPanelEditar.add(txtcantidadH,1,1);
					gPanelEditar.add(aceptarEdit,1,2);
					gPanelEditar.add(cancel,2,2);
					gPanelEditar.setAlignment(Pos.CENTER);
					gPanelEditar.setVgap(8);
					gPanelEditar.setHgap(8);
					bpPrincipal.setCenter(gPanelEditar);
				}
			}else if(event.getSource()==cancel){
				bpPrincipal.setCenter(null);
			}else if(event.getSource()==aceptarEdit){
				editarHerramienta();
			}else if(event.getSource()==eliminarHer){
				Herramienta herramientaSel = (Herramienta)tvHerramientas.getSelectionModel().getSelectedItem();
				if(herramientaSel!=null){
					mHerramienta.eliminarHerramienta(herramientaSel.getIdHerramienta());
				}
			}else if(event.getSource()==editarU){
				usuarioSe=(Usuario)	tvUsuario.getSelectionModel().getSelectedItem();
				if(usuarioSe!=null){
					gPanelEditar=new GridPane();
					pfContra=new PasswordField();
					pfContra.addEventHandler(KeyEvent.KEY_RELEASED,this);
					pfContra2=new PasswordField();
					pfContra.addEventHandler(KeyEvent.KEY_RELEASED,this);
					lblPassword=new Label("Password: ");
					lblPassword2=new Label("Vuelve a escribirla: ");
					aceptarEditU=new Button("Editar");
					cancel.addEventHandler(ActionEvent.ACTION,this);
					aceptarEditU.addEventHandler(ActionEvent.ACTION,this);
					gPanelEditar.add(lblPassword,0,0);
					gPanelEditar.add(pfContra,1,0);
					gPanelEditar.add(lblPassword2,0,1);
					gPanelEditar.add(pfContra2,1,1);
					gPanelEditar.add(aceptarEditU,1,2);
					gPanelEditar.add(cancel,2,2);
					gPanelEditar.setAlignment(Pos.CENTER);
					gPanelEditar.setVgap(8);
					gPanelEditar.setHgap(8);
					bpPrincipal.setCenter(gPanelEditar);
				}
			}else if(event.getSource()==aceptarEditU){
				System.out.println(2);
				editarUsuario();
			}else if(event.getSource()==eliminarU){
				Usuario userEl=(Usuario)tvUsuario.getSelectionModel().getSelectedItem();
				if(userEl!=null){
					if(userEl.getIdUsuario()!=mUsuario.getUsuarioAutenticado().getIdUsuario()){
						mUsuario.eliminarUsuario(userEl);
						mPrestamo.actualizarPrestamo();
					}
				}
			}else if(event.getSource()==editarC){
				clienteSe=(Cliente)	tvCliente.getSelectionModel().getSelectedItem();
				if(clienteSe!=null){
					gPanelEditar=new GridPane();
					txtNombreC=new TextField();
					lblNombreC=new Label("Nombre del Profesor:");
					txtNombreC.addEventHandler(KeyEvent.KEY_RELEASED,this);
					aceptarEditC=new Button("Editar");
					cancel.addEventHandler(ActionEvent.ACTION,this);
					aceptarEditC.addEventHandler(ActionEvent.ACTION,this);
					gPanelEditar.add(lblNombreC,0,0,2,1);
					gPanelEditar.add(txtNombreC,0,1,2,1);
					gPanelEditar.add(aceptarEditC,0,2);
					gPanelEditar.add(cancel,1,2);
					gPanelEditar.setAlignment(Pos.CENTER);
					gPanelEditar.setVgap(8);
					gPanelEditar.setHgap(8);
					bpPrincipal.setCenter(gPanelEditar);
				}
			}else if(event.getSource()==aceptarEditC){
				editarCliente();
			}else if(event.getSource()==eliminarC){
				Cliente eliminarClie=(Cliente)tvCliente.getSelectionModel().getSelectedItem();
				if(eliminarClie!=null){
					mCliente.eliminarCliente(eliminarClie.getIdCliente());
					mPrestamo.actualizarPrestamo();
				}
			}
		}else if(event instanceof KeyEvent){
			if(event.getSource()==txtcantidadH||event.getSource()==txtnombreH){
				if(((KeyEvent)event).getCode() == KeyCode.ENTER){
					editarHerramienta();
				}
			}else if(event.getSource()==pfContra||event.getSource()==pfContra2){
				if(((KeyEvent)event).getCode() == KeyCode.ENTER){
					editarUsuario();
				}
			}else if(event.getSource()==txtNombreC){
				if(((KeyEvent)event).getCode() == KeyCode.ENTER){
					editarCliente();
				}
			}
		}
	}
	/**
		Edita el cliente de la db
	*/
	public void editarCliente(){
		if(!txtNombreC.getText().trim().equals("")){
			mCliente.actualizarCliente(txtNombreC.getText(),clienteSe.getIdCliente());
			mPrestamo.actualizarPrestamo();
			bpPrincipal.setCenter(null);
		}else{
			lblErr.setText("Falta rellenar algun dato.");
		}
		try{
			gPanelEditar.add(lblErr,1,3,2,3);
		}catch(IllegalArgumentException e){

		}
	}
	/**
		Edita el usuario de la db
	*/
	public void editarUsuario(){
		if(!pfContra.getText().trim().equals("") && !pfContra2.getText().trim().equals("")){
			if(pfContra.getText().equals(pfContra2.getText())){
				mUsuario.actualizarUsuario("passw",pfContra.getText(),usuarioSe.getIdUsuario(),usuarioSe.getIdEncargado());
				mPrestamo.actualizarPrestamo();
				bpPrincipal.setCenter(null);
			}else{
				lblErr.setText("Las password deben ser iguales.");
			}
		}else{
			lblErr.setText("Falta rellenar algun dato.");
		}
		try{
			gPanelEditar.add(lblErr,1,3,2,3);
		}catch(IllegalArgumentException e){

		}
	}
	/**
		Edita la herramienta en la db
	*/
	public void editarHerramienta(){		
		if(!txtcantidadH.getText().trim().equals("") && !txtnombreH.getText().trim().equals("")){
			gPanelEditar.getChildren().remove(lblErr);
			boolean verificar=true;
			ArrayList<Herramienta> listaH=new ArrayList<Herramienta>(mHerramienta.getListaHerramientas());
			for(Herramienta her:listaH){
				if(!herramientaSe.getNombre().equalsIgnoreCase(txtnombreH.getText())){
					if(her.getNombre().equalsIgnoreCase(txtnombreH.getText()))
						verificar=false;
				}
			}
			String cant=""+herramientaSe.getCantidad();
			if(herramientaSe.getNombre().equals(txtnombreH.getText()) && cant.equals(txtcantidadH.getText())){
				bpPrincipal.setCenter(null);
			}else{
				if(verificar){
					try{	
						verificar=true;
						int cantidad=Integer.parseInt(txtcantidadH.getText());
						mHerramienta.actualizarAmbosCampos(txtnombreH.getText(),cantidad,herramientaSe.getIdHerramienta());
						herramientaSe.setNombre(txtnombreH.getText());
						herramientaSe.setCantidad(cantidad);
						lblErr.setText("Herramienta editada correctamente.");
					}catch(NumberFormatException fe){
						lblErr.setText("La cantidad debe ser numerica.");
					}
				}else{
					lblErr.setText("Ya hay una herramienta con ese nombre.");	
				}
			}
		}else{
			lblErr.setText("Falta rellenar algun dato.");
		}
		try{
			gPanelEditar.add(lblErr,1,3,2,3);
		}catch(IllegalArgumentException e){

		}
	}
}