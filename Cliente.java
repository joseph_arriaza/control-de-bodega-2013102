package org.jarriaza.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
/**
	Beans cliente
*/
public class Cliente{
	private StringProperty nombre;
	private IntegerProperty idCliente;
	/**
		Constructor vacio
	*/
	public Cliente(){
		this.init();
	}
	/**
		Constructor lleno
		@param idCliente
		@param nombre
	*/
	public Cliente(int idCliente , String nombre){
		this.init();
		this.setIdCliente(idCliente);	
		this.setNombre(nombre);
	}
	/**
		Inicializa las variables
	*/
	private void init(){
		this.idCliente = new SimpleIntegerProperty();
		this.nombre = new SimpleStringProperty();
	}
	/**
		Devuelve el nombre
		@return nombre
	*/
	public String getNombre(){
		return this.nombre.get();
	}
	/**
		Ingresa el nombre
		@param nombre
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}/**
		Devuelve el nombre observable
		@return nombre
	*/
	public StringProperty nombreProperty(){
		return this.nombre;
	}
	/**
		Devuelve el idClinete
		@return idCliente
	*/
	public int getIdCliente(){
		return this.idCliente.get();
	}
	/**
		Ingresa el idClinete
		@param idCliente
	*/
	public void setIdCliente(int id){
		this.idCliente.set(id);
	}
	/**
		Devuelve el idClinete observable
		@return idCliente
	*/
	public IntegerProperty idClienteProperty(){
		return this.idCliente;
	}
}
