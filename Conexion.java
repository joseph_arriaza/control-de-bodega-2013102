package org.jarriaza.db;

import org.jarriaza.utilidad.ReadText;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
	Clase que conecta con la db
*/
public class Conexion{
	private Connection cnx;
	private Statement stn;
	/**
		Constructor donde se hace la conexion por un archivo .conf
	*/
	public Conexion(){
		try{
			String conf[]=new ReadText().conf();
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			cnx = DriverManager.getConnection("jdbc:sqlserver://"+conf[0]+"\\"+conf[4]+":1433;databaseName="+conf[1]+";", ""+conf[2]+"", ""+conf[3]+"");
			stn = cnx.createStatement();	
		}catch(SQLException sql){
			sql.printStackTrace();
		}catch(ClassNotFoundException cl){
			System.out.println("Driver no encontrado");
		}
	}
	/**
		Ejecuta una sentencia SQL
		@param sentencia
	*/
	public void ejecutarSentencia(String sentencia){
		try{
			stn.execute(sentencia);
		}catch(SQLException sq){
			sq.printStackTrace();
		}
	}
	/**
		Ejecuta una consulta y devuelve el resultado
		@param consulta
		@return resultado
	*/
	public ResultSet ejecutarConsulta(String consulta){
		ResultSet resultado = null;
		try{
			resultado = stn.executeQuery(consulta);
		}catch(SQLException sq){
			sq.printStackTrace();
		}
		return resultado;
	}
	
}
