package org.jarriaza.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
/**
	Beans Herramienta
*/
public class Herramienta{
	private StringProperty nombre;
	private IntegerProperty idHerramienta,cantidad;
	/**
		Constructor vacio
	*/
	public Herramienta(){
		this.init();
	}
	/**
		Constructor con parametros
		@param itHerramienta
		@param nombre
		@param cantidad
	*/
	public Herramienta(int idHerramienta , String nombre, int cantidad){
		this.init();
		this.setIdHerramienta(idHerramienta);	
		this.setNombre(nombre);	
		this.setCantidad(cantidad);		
	}
	/**
		Inicializa las variables
	*/
	private void init(){
		this.idHerramienta = new SimpleIntegerProperty();
		this.nombre = new SimpleStringProperty();
		this.cantidad = new SimpleIntegerProperty();
	}
	/**
		Devuelve el nombre
		@return nombre
	*/
	public String getNombre(){
		return this.nombre.get();
	}
	/**
		Ingresa el nombre
		@param nombre
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	/**
		Devuelve el nombre observable
		@return nombre
	*/
	public StringProperty nombreProperty(){
		return this.nombre;
	}
	/**
		Devuelve el idHerramienta
		@return idHerramienta
	*/
	public int getIdHerramienta(){
		return this.idHerramienta.get();
	}
	/**
		Ingreas el idHerramienta
		@return idHerramienta
	*/
	public void setIdHerramienta(int id){
		this.idHerramienta.set(id);
	}
	/**
		Devuelve el idHerramienta observable
		@return idHerramienta
	*/
	public IntegerProperty idHerramientaProperty(){
		return this.idHerramienta;
	}
	/**
		Devuelve la cantidad
		@return cantidad
	*/
	public int getCantidad(){
		return this.cantidad.get();
	}
	/**
		Ingresa la cantidad
		@param cantidad
	*/
	public void setCantidad(int id){
		this.cantidad.set(id);
	}
	/**
		Devuelve la cantidad observable
		@return cantidad
	*/
	public IntegerProperty cantidadProperty(){
		return this.cantidad;
	}
}
