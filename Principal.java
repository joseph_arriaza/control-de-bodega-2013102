package org.jarriaza.ui;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Menu;
import javafx.scene.control.TableCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.geometry.Pos;
import javafx.geometry.Insets;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.Event;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.Scene;

import javafx.util.Callback;
import java.util.ArrayList;
import javafx.scene.paint.Color;

import org.jarriaza.manejadores.ManejadorUsuario;
import org.jarriaza.manejadores.ManejadorCliente;
import org.jarriaza.manejadores.ManejadorEncargado;
import org.jarriaza.manejadores.ManejadorPrestamo;
import org.jarriaza.manejadores.ManejadorHerramienta;
import org.jarriaza.manejadores.ManejadorDetallePrestamo;
import org.jarriaza.db.Conexion;
import org.jarriaza.beans.Cliente;
import org.jarriaza.beans.Prestamo;
import org.jarriaza.beans.Usuario;
import org.jarriaza.beans.DetallePrestamo;

import org.jarriaza.utilidad.VerPrestamo;
import org.jarriaza.utilidad.AgregarTab;
import org.jarriaza.utilidad.AgregarTableView;
/**
	Clase donde se encuentran la mayoria de funciones
*/
public class Principal extends Application implements EventHandler<Event>{

	private Scene primaryScene;
	
	private TableView tvPrestamos, tvVerPrestamos;
	
	private TabPane tpPrincipal;
	private Tab tbLogin,tbStart,tbPrestamo,tbAgregarHerramienta;

	private Button btnLogin, btnLogout, btnVerPrestamo,entregado;
	private TextField tfNombre;
	private PasswordField pfContra;
	private Label lblNombre, lblContra, lblErr;
	private Conexion cnx;
	
	private ToolBar tbHerramientas;
	
	private MenuItem mItem,mItemVerHerramientas,mItemPrestamoAdd,mItemVerDevoluciones,mItemAgregarUsuario,mItemListarUsuario,mItemAgregarCliente,mItemListarCliente;
	private MenuBar mBarActions;
	private Menu mnHerramientas,menPrestamo,mnUsuario,mnCliente;

	private GridPane gpLogin,gpStart,gpPrestamo;
	private BorderPane bPanelStart, bPanelPrestamo, bpInicio;
	
	private ManejadorUsuario mUsuario;
	private ManejadorEncargado mEncargado;
	private ManejadorHerramienta mHerramientas;
	private ManejadorPrestamo mPrestamo;
	private ManejadorDetallePrestamo mDetallePrestamo;
	/**
		Metodo start del proyecto
	*/
	public void start(Stage primaryStage){
		cnx = new Conexion();
		
		mUsuario = new ManejadorUsuario(cnx);
		mEncargado = new ManejadorEncargado(cnx);
		mPrestamo = new ManejadorPrestamo(cnx);
		tbHerramientas = new ToolBar();
		mItem = new MenuItem("Agregar Herramienta");
		mItem.setId("1");
		ejecutarMenus(mItem);
		mItemVerHerramientas=new MenuItem("Ver herramientas");
		mItemVerHerramientas.setId("a");
		ejecutarMenus(mItemVerHerramientas);
		mItemPrestamoAdd = new MenuItem("Agregar Prestamo");
		mItemPrestamoAdd.setId("2");
		ejecutarMenus(mItemPrestamoAdd);
		mItemVerDevoluciones= new MenuItem("Ver Devoluciones");
		mItemVerDevoluciones.setId("3");
		ejecutarMenus(mItemVerDevoluciones);
		mItemAgregarUsuario = new MenuItem("Agregar Usuario");
		mItemAgregarUsuario.setId("4");
		ejecutarMenus(mItemAgregarUsuario);
		mItemListarUsuario = new MenuItem("Ver Usuarios");
		mItemListarUsuario.setId("b");
		ejecutarMenus(mItemListarUsuario);
		mItemAgregarCliente = new MenuItem("Agregar Profesor");
		mItemAgregarCliente.setId("5");
		ejecutarMenus(mItemAgregarCliente);
		mItemListarCliente = new MenuItem("Ver Profesores");
		mItemListarCliente.setId("c");
		ejecutarMenus(mItemListarCliente);
		
		mBarActions = new MenuBar();
		menPrestamo = new Menu("Prestamos");
		mnHerramientas = new Menu("Herramientas");
		mnUsuario = new Menu("Usuarios");
		mnCliente = new Menu("Profesores");
		bPanelStart = new BorderPane();
		bPanelPrestamo = new BorderPane();
		bpInicio = new BorderPane();
		
		tvPrestamos=new TableView();
		
		tpPrincipal = new TabPane();
		
		entregado=new Button("Entregado");
		entregado.addEventHandler(ActionEvent.ACTION,this);
		btnLogout = new Button("Logout");
		btnLogout.addEventHandler(ActionEvent.ACTION,this);
		btnVerPrestamo = new Button("Ver Prestamo");
		btnVerPrestamo.addEventHandler(ActionEvent.ACTION,this);			
		//mBarActions.getMenus().addAll(menPrestamo,mnHerramientas);
		//tbHerramientas.getItems().addAll(mBarActions,entregado,btnLogout);
		mnHerramientas.getItems().addAll(mItem,mItemVerHerramientas);
		mnUsuario.getItems().addAll(mItemAgregarUsuario,mItemListarUsuario);
		mnCliente.getItems().addAll(mItemAgregarCliente,mItemListarCliente);
		menPrestamo.getItems().addAll(mItemPrestamoAdd);		
		
		btnLogin = new Button("Iniciar");
		btnLogin.addEventHandler(ActionEvent.ACTION,this);

		tbLogin = new Tab("Login");
		tbStart = new Tab("Principal");
		tbPrestamo = new Tab("Prestamo");

		gpLogin = new GridPane();
		gpStart = new GridPane();
		gpPrestamo = new GridPane();

		tfNombre = new TextField();
		tfNombre.setPromptText("username");
		tfNombre.addEventHandler(KeyEvent.KEY_RELEASED, this);

		pfContra = new PasswordField();
		pfContra.setPromptText("password");
		pfContra.addEventHandler(KeyEvent.KEY_RELEASED, this);

		lblNombre = new Label("Username : ");
		lblContra = new Label("Password : ");
		lblErr = new Label("Verifique sus credenciales.");

		gpLogin.add(lblNombre, 0,0);
		gpLogin.add(tfNombre, 1, 0);
		gpLogin.add(lblContra, 0,1);
		gpLogin.add(pfContra, 1, 1);
		gpLogin.add(btnLogin,1,2);
		gpLogin.setVgap(5);
		gpLogin.setMargin(btnLogin, new Insets(0,0,0,100));

		tvPrestamos = new TableView<Prestamo>(mPrestamo.getListaPrestamo());
		tvPrestamos.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

		TableColumn<Prestamo, String> columnaProfesor=new TableColumn<Prestamo, String>("PROFESOR");
		columnaProfesor.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("profesor"));
		columnaProfesor.setPrefWidth(85);
		TableColumn<Prestamo, String> columnaIdEncargado=new TableColumn<Prestamo, String>("ENCARGADO");
		columnaIdEncargado.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("encargado"));
		columnaIdEncargado.setPrefWidth(85);
		TableColumn<Prestamo, String> columnaCodigo=new TableColumn<Prestamo, String>("CODIGO SECCION");
		columnaCodigo.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("codigoSecc"));
		columnaCodigo.setPrefWidth(110);
		TableColumn<Prestamo, String> columnaHora=new TableColumn<Prestamo, String>("HORA");
		columnaHora.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("hora"));
		columnaHora.setPrefWidth(75);
		TableColumn<Prestamo, String> columnaFecha=new TableColumn<Prestamo, String>("FECHA");
		columnaFecha.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("fecha"));
		columnaFecha.setPrefWidth(75);
		TableColumn columnaEstado = new TableColumn("ESTADO");  
    	columnaEstado.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("nombreEstado"));
    	
		columnaEstado.setPrefWidth(100);
		columnaEstado.setCellFactory(new Callback<TableColumn, TableCell>() {
			/**
				Metodo que cambia el color de un row
				@param param
				@return tableCell
			*/
        	public TableCell call(TableColumn param) {
            	return new TableCell<Prestamo, String>() {
                	@Override
                	public void updateItem(String item, boolean empty) { 
                    	super.updateItem(item, empty); 
                    	if (!isEmpty()) {
                        	if(item.contains("Fuera de Fecha")){
                        		this.getTableRow().setStyle("-fx-background-color:red");
                        	}else if(item.contains("Entregado")){
                        		this.getTableRow().setStyle("-fx-background-color:green");
                        	}else if(item.contains("Pendiente")){
                        		this.getTableRow().setStyle("-fx-background-color:orange");
                        	}
                        	setText(item);
                    	}
	                }
    	        };
        	}
    	});
		tvPrestamos.getColumns().setAll(columnaProfesor,columnaIdEncargado,columnaCodigo, columnaHora, columnaFecha, columnaEstado);
		tvPrestamos.setPrefWidth(532);

		gpLogin.setAlignment(Pos.CENTER);		
		bPanelStart.setCenter(gpLogin);
		tbLogin.setContent(bPanelStart);	
		tbLogin.setClosable(false);
		
		tpPrincipal.getTabs().add(tbLogin);
		bpInicio.setCenter(tpPrincipal);

		primaryScene = new Scene(bpInicio,750,400);
		primaryScene.getStylesheets().add("style.css");
		primaryStage.setTitle("Bodega");
		primaryStage.setScene(primaryScene);

		primaryStage.show();
	}
	/**
		Metodo que ejecuta la funcion de los menos
		@param menuItem
	*/
	private void ejecutarMenus(MenuItem menuItem){	
	AgregarTab addTab=new AgregarTab();	
		menuItem.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {				
            	try{
            		Integer.parseInt(menuItem.getId());
               		tbAgregarHerramienta= addTab.agregar(menuItem,mUsuario,cnx,mPrestamo);	
               	}catch(NumberFormatException nfe){
               		AgregarTableView tbView=new AgregarTableView();
               		if(menuItem.getId().equals("a"))
               			tbAgregarHerramienta= tbView.agregarTable("herramientas",cnx,mUsuario,mPrestamo);	
					else if(menuItem.getId().equals("b"))
						tbAgregarHerramienta= tbView.agregarTable("usuario",cnx,mUsuario,mPrestamo);	
					else if(menuItem.getId().equals("c"))
						tbAgregarHerramienta= tbView.agregarTable("cliente",cnx,mUsuario,mPrestamo);	
               	}
				tpPrincipal.getTabs().add(tbAgregarHerramienta);
				tpPrincipal.getSelectionModel().select(tbAgregarHerramienta);								
            }
        }); 		
	}
	/**
		Verifica que los datos no sean nulos
	*/
	private boolean verificarDatos(){
		return !tfNombre.getText().trim().equals("") & !pfContra.getText().trim().equals("");
	}
	/**
		Metodo handle obligado a ser implemento
		@param event
	*/
	public void handle(Event event){
		if(event instanceof KeyEvent){
			if(((KeyEvent)event).getCode() == KeyCode.ENTER){
				iniciar();
			}			
		}else if(event instanceof ActionEvent){
			if(event.getSource().equals(btnLogout)){				
				mEncargado.setUsuarioConectado(null);
				mUsuario.desautenticar();
				bPanelStart.setTop(null);
				bPanelStart.setCenter(gpLogin);
				tpPrincipal.getTabs().clear();	
				tpPrincipal.getTabs().add(tbLogin);											
				gpStart.getChildren().clear();
				bpInicio.getChildren().remove(tbHerramientas);
				tfNombre.clear();
				pfContra.clear();
				gpLogin.getChildren().remove(lblErr);
			}else if(event.getSource()==btnLogin){
				iniciar();
			}else if(event.getSource()==btnVerPrestamo){
				Prestamo prestamo = (Prestamo)tvPrestamos.getSelectionModel().getSelectedItem();
				if(prestamo!=null){
					gpPrestamo.getChildren().clear();
					tvVerPrestamos=new VerPrestamo().prestamo(prestamo, cnx, mUsuario, prestamo.getIdCliente(),prestamo.getIdPrestamo());
					gpPrestamo.add(tvVerPrestamos,0,0);
					bPanelPrestamo.setCenter(gpPrestamo);
					tbPrestamo.setContent(bPanelPrestamo);
					tpPrincipal.getSelectionModel().select(tbPrestamo);
					tpPrincipal.getTabs().add(tbPrestamo);			
				}
			}else if(event.getSource()==entregado){
				Prestamo prestamo = (Prestamo)tvPrestamos.getSelectionModel().getSelectedItem();
				if(prestamo!=null){
					if(prestamo.getIdEstado()!=2){							
						gpPrestamo.getChildren().clear();
						mHerramientas=new ManejadorHerramienta(cnx);
						mDetallePrestamo=new ManejadorDetallePrestamo(cnx);
						mHerramientas.setUsuarioConectado(mUsuario.getUsuarioAutenticado());
						mDetallePrestamo.setUsuarioConectado(mUsuario.getUsuarioAutenticado(),prestamo.getIdCliente(),prestamo.getIdPrestamo());
						ArrayList<DetallePrestamo> listadedetalles=new ArrayList<>(mDetallePrestamo.getListaDetallePrestamo());
						for(DetallePrestamo dPrestamo:listadedetalles){
							if(dPrestamo.getIdPrestamo()==prestamo.getIdPrestamo()){
								mHerramientas.actualizarHerramienta("cantidad","",dPrestamo.getIdHerramienta(),dPrestamo.getCantidad());
							}
						}					
						mPrestamo.actualizarPrestamos("idEstado","2",prestamo.getIdPrestamo());		
					}			
				}
			}
		}
	}
	/**
		Metodo que inicia sesion
	*/
	public void iniciar(){
		if(verificarDatos()){
			if(mUsuario.autenticar(tfNombre.getText(), pfContra.getText())){
				tpPrincipal.getTabs().add(tbStart);
				tpPrincipal.getTabs().remove(tbLogin);
				bpInicio.setTop(tbHerramientas);
				tbStart.setClosable(false);	
				tbStart.setContent(gpStart);
				mPrestamo.setUsuarioConectado(mUsuario.getUsuarioAutenticado());
				gpStart.add(tvPrestamos,0,0);
				gpStart.add(btnVerPrestamo,1,0);				
				gpStart.setHgap(8);
				agregarPermisos();
				mEncargado.setUsuarioConectado(mUsuario.getUsuarioAutenticado());
			}else{				
				try{
					lblErr.setText("Verifica tus datos.");
					gpLogin.add(lblErr,1,3);
				}catch(IllegalArgumentException npe){
				
				}
			}
		}else{
			try{
				lblErr.setText("Falta rellenar algun dato.");
				gpLogin.add(lblErr,1,3);
			}catch(IllegalArgumentException npe){
			
			}
		}
	}
	public void agregarPermisos(){
		try{
			mBarActions.getMenus().clear();
			tbHerramientas.getItems().clear();
			if(mUsuario.getUsuarioAutenticado().getRango()==1){ //admin
				mBarActions.getMenus().addAll(menPrestamo,mnHerramientas,mnUsuario,mnCliente);
				tbHerramientas.getItems().addAll(mBarActions,entregado,btnLogout);
			}else if(mUsuario.getUsuarioAutenticado().getRango()==2){ //modulo crud
				mBarActions.getMenus().addAll(mnHerramientas,mnUsuario,mnCliente);
				tbHerramientas.getItems().addAll(mBarActions,btnLogout);
			}else if(mUsuario.getUsuarioAutenticado().getRango()==3){ //modulo prestamista
				mBarActions.getMenus().addAll(menPrestamo);
				tbHerramientas.getItems().addAll(mBarActions,btnLogout);
			}else if(mUsuario.getUsuarioAutenticado().getRango()==4){ //modulo visor
				tbHerramientas.getItems().addAll(entregado,btnLogout);
			}else if(mUsuario.getUsuarioAutenticado().getRango()==5){ //modulo crud y prestamista
				mBarActions.getMenus().addAll(menPrestamo,mnHerramientas,mnUsuario,mnCliente);
				tbHerramientas.getItems().addAll(mBarActions,btnLogout);
			}else if(mUsuario.getUsuarioAutenticado().getRango()==6){ //modulo crud y visor
				mBarActions.getMenus().addAll(mnHerramientas,mnUsuario,mnCliente);
				tbHerramientas.getItems().addAll(mBarActions,btnLogout);
			}else if(mUsuario.getUsuarioAutenticado().getRango()==7){ //modulo visor y prestamista
				mBarActions.getMenus().addAll(menPrestamo);
				tbHerramientas.getItems().addAll(mBarActions,btnLogout);
			}
		}catch(Exception e){
		
		}
	}
	/**
		Ingreas un usuario conectado
		@param mUsuario
	*/
	public void setMUsuario(ManejadorUsuario mUsuario){
		this.mUsuario = mUsuario;
	}
	/**
		Devuelve el tpPrincipal
		@return tpPrincipal
	*/
	public TabPane getTpPrincipal(){
		return this.tpPrincipal;
	}
	/**
		Inserta el tpPrincipal
		@param tpPrincipal inserta el tab pane Principal
	*/
	public void setTpPrincipal(TabPane tpPrincipal){
		this.tpPrincipal=tpPrincipal;
	}
	/**
		Devuelve el tbAgregarHerramienta
		@return tbAgregarHerramienta
	*/
	public Tab getTbAgregarHerramienta(){
		return this.tbAgregarHerramienta;
	}
}