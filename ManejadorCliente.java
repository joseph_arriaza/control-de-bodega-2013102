package org.jarriaza.manejadores;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import java.sql.SQLException;
import java.sql.ResultSet;

import org.jarriaza.beans.Cliente;
import org.jarriaza.beans.Usuario;
import org.jarriaza.db.Conexion;
/**
	Manejador del beans clinete
*/
public class ManejadorCliente{
	private ObservableList<Cliente> listaCliente;
	private Conexion cnx;
	private Usuario usuarioConectado;
	/**
		instancia la lsita
		@param cnx
	*/
	public ManejadorCliente(Conexion cnx){
		this.listaCliente = FXCollections.observableArrayList();
		this.cnx = cnx;
	}
	/**
		Ingresa el usuairo conectado
		@param usuarioConectado
	*/
	public void setUsuarioConectado(Usuario usuarioConectado){
		this.usuarioConectado=usuarioConectado;
		actualizarClientes();
	}
	/**
		Obtiene el usuario conectado
		@return usuarioConectado
	*/
	public Usuario getUsuarioConectado(){
		return this.usuarioConectado;
	}
	/**
		Actualizar lista de clientes
	*/
	public void actualizarClientes(){
		if(usuarioConectado!=null){
			ResultSet resultado = cnx.ejecutarConsulta("SELECT idCliente, nombre  FROM Cliente");
			if(resultado!=null){
				listaCliente.clear();
				try{
					while(resultado.next()){
						Cliente cliente=new Cliente(resultado.getInt("idCliente"),resultado.getString("nombre"));
						listaCliente.add(cliente);
					}
				}catch(SQLException sql){
					sql.printStackTrace();
				}
			}
		}		
	}
	public ObservableList<Cliente> getlistaClientes(){
		return listaCliente;
	}
	/**
		Agregar Cliente
		@param cliente
	*/
	public void agregarCliente(Cliente cliente){
		cnx.ejecutarSentencia("INSERT INTO Cliente(nombre) VALUES ('"+cliente.getNombre()+"')");
		actualizarClientes();
		
	}
	/**
		Eliminar Clinete
		@param idCliente
	*/
	public void eliminarCliente(int idCliente){
		cnx.ejecutarSentencia("EXEC EliminarProfesor "+idCliente);
		actualizarClientes();
	}
	/**
		Actualiza un cliente
		@param campo
		@param valor
		@param idCliente
	*/
	public void actualizarCliente(String valor,int idCliente){
		cnx.ejecutarSentencia("UPDATE Cliente SET nombre='"+valor+"' WHERE idCliente="+idCliente);
		actualizarClientes();
	}
}