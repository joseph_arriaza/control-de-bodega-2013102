package org.jarriaza.utilidad;

import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;

import org.jarriaza.db.Conexion;
import org.jarriaza.beans.Prestamo;
import org.jarriaza.beans.DetallePrestamo;
import org.jarriaza.manejadores.ManejadorUsuario;
import org.jarriaza.manejadores.ManejadorCliente;
import org.jarriaza.manejadores.ManejadorPrestamo;
import org.jarriaza.manejadores.ManejadorDetallePrestamo;
/**
	Clase que ve los articulos de un prestamo
*/
public class VerPrestamo{
	private TableView tvPrestamos;
	private TableColumn estadoColumna, herramientaColumna, cantidadColumna, fechaColumna, horaColumna;
	private ManejadorDetallePrestamo mDetallePrestamo;
	/**
		Ve los articulso prestados de algun cliente
		@param prestamo
		@param cnx
		@param mUsuario
		@param idCliente
		@return tvPrestamo
	*/
	public TableView prestamo(Prestamo prestamo, Conexion cnx, ManejadorUsuario mUsuario, int idCliente,int idPrestamo){
		mDetallePrestamo=new ManejadorDetallePrestamo(cnx);
		mDetallePrestamo.setUsuarioConectado(mUsuario.getUsuarioAutenticado(),idCliente,idPrestamo);

		tvPrestamos = new TableView<DetallePrestamo>(mDetallePrestamo.getListaDetallePrestamo());	

		TableColumn<DetallePrestamo, String> herramientaColumna=new TableColumn<DetallePrestamo, String>("HERRAMIENTA");
		herramientaColumna.setCellValueFactory(new PropertyValueFactory<DetallePrestamo, String>("herramienta"));
		herramientaColumna.setPrefWidth(105);
		TableColumn<DetallePrestamo, String> cantidadColumna=new TableColumn<DetallePrestamo, String>("CANTIDAD");
		cantidadColumna.setCellValueFactory(new PropertyValueFactory<DetallePrestamo, String>("cantidad"));
		cantidadColumna.setPrefWidth(75);
		TableColumn<DetallePrestamo, String> fechaColumna=new TableColumn<DetallePrestamo, String>("FECHA");
		fechaColumna.setCellValueFactory(new PropertyValueFactory<DetallePrestamo, String>("fecha"));
		fechaColumna.setPrefWidth(85);
		TableColumn<DetallePrestamo, String> horaColumna=new TableColumn<DetallePrestamo, String>("HORA");
		horaColumna.setCellValueFactory(new PropertyValueFactory<DetallePrestamo, String>("hora"));
		horaColumna.setPrefWidth(95);

		tvPrestamos.getColumns().setAll(herramientaColumna,cantidadColumna,fechaColumna,horaColumna);
		tvPrestamos.setPrefWidth(363);

		return tvPrestamos;
	}
}