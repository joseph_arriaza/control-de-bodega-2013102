package org.jarriaza.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
/**
	Beans usuario
*/
public class Usuario{
	private StringProperty nombre,username,passw;
	private IntegerProperty idUsuario, rango, idEncargado;
	/**
		Constructor vacio
	*/
	public Usuario(){
		this.init();
	}
	/**
		Constructor lleno
		@param idUsuario
		@param nombre
		@param username
		@param passw
		@param rango
		@param idEncargado
	*/
	public Usuario(int idUsuario , String nombre, String username, String passw, int rango, int idEncargado){
		this.init();
		this.setIdEncargado(idEncargado);
		this.setIdUsuario(idUsuario);	
		this.setNombre(nombre);
		this.setUsername(username);
		this.setPassw(passw);
		this.setRango(rango);	
	}
	/**
		Inicializa las variables
	*/
	private void init(){
		this.idEncargado = new SimpleIntegerProperty();
		this.idUsuario = new SimpleIntegerProperty();
		this.rango = new SimpleIntegerProperty();
		this.nombre = new SimpleStringProperty();
		this.username = new SimpleStringProperty();
		this.passw= new SimpleStringProperty();
	}
	/**
		Devuelve el nombre
		@return nombre
	*/
	public String getNombre(){
		return this.nombre.get();
	}
	/**
		Ingresa el nombre
		@param nombre
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	/**
		Devuelve el nombre observable
		@return nombre
	*/
	public StringProperty nombreProperty(){
		return this.nombre;
	}
	/**
		Devuelve el idUsuario
		@return idUsuario
	*/
	public int getIdUsuario(){
		return this.idUsuario.get();
	}
	/**
		Ingresa el idUsuario
		@param idUsuario
	*/
	public void setIdUsuario(int id){
		this.idUsuario.set(id);
	}
	/**
		Devuelve el idUsuario observable
		@return idUsuario
	*/
	public IntegerProperty idUsuarioProperty(){
		return this.idUsuario;
	}
	/**
		Devuelve el username
		@return username
	*/
	public String getUsername(){
		return this.username.get();
	}
	/**
		Ingresa el username
		@param username
	*/
	public void setUsername(String username){
		this.username.set(username);
	}
	/**
		Devuelve el username observable
		@return username
	*/
	public StringProperty usernameProperty(){
		return this.username;
	}
	/**
		Devuelve el passw
		@return passw
	*/
	public String getPassw(){
		return this.passw.get();
	}
	/**
		Ingresa el passw
		@param passw
	*/
	public void setPassw(String passw){
		this.passw.set(passw);
	}
	/**
		Devuelve el passw observable
		@return passw
	*/
	public StringProperty passwProperty(){
		return this.passw;
	}
	/**
		Devuelve el rango
		@return rango
	*/
	public int getRango(){
		return this.rango.get();
	}
	/**
		Ingresa el rango
		@param rango
	*/
	public void setRango(int rango){
		this.rango.set(rango);
	}
	/**
		Devuelve el rango observable
		@return rango
	*/
	public IntegerProperty rangoProperty(){
		return this.rango;
	}
	/**
		Devuelve el idEncargado
		@return idEncargado
	*/
	public int getIdEncargado(){
		return this.idEncargado.get();
	}
	/**
		Ingresa el idEncargado
		@param idEncargado
	*/
	public void setIdEncargado(int id){
		this.idEncargado.set(id);
	}
	/**
		Devuelve el idEncargado observable
		@return idEncargado
	*/
	public IntegerProperty idEncargadoProperty(){
		return this.idEncargado;
	}
}