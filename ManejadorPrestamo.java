package org.jarriaza.manejadores;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.Date;
import java.text.SimpleDateFormat;
import org.jarriaza.beans.Prestamo;
import org.jarriaza.beans.Usuario;
import org.jarriaza.db.Conexion;
import java.time.LocalDateTime;
import java.util.GregorianCalendar;
import java.util.Calendar;
import org.jarriaza.manejadores.ManejadorCliente;
import org.jarriaza.utilidad.VerificarFecha;
/**
	Manejador del beans prestamo
*/
public class ManejadorPrestamo{
	private ObservableList<Prestamo> listaPrestamo;
	private Conexion cnx;
	private Usuario usuarioConectado;
	private VerificarFecha veFecha;
	/**
		Instancia la lista observable
		@param cnx
	*/
	public ManejadorPrestamo(Conexion cnx){
		this.listaPrestamo = FXCollections.observableArrayList();
		veFecha=new VerificarFecha();
		this.cnx = cnx;
	}
	/**
		Ingresa un usuario conectado
		@param usuarioConectado
	*/
	public void setUsuarioConectado(Usuario usuarioConectado){
		this.usuarioConectado=usuarioConectado;
		actualizarPrestamo();
	}
	/**
		Obtiene un usuario conectado
		@return usuarioConectado
	*/
	public Usuario getUsuarioConectado(){
		return this.usuarioConectado;
	}
	/**
		Actualiza la lista de prestamos
	*/
	public void actualizarPrestamo(){
		if(usuarioConectado!=null){
			ResultSet resultado = cnx.ejecutarConsulta("SELECT Cliente.nombre AS Cliente, Estado.nombre AS NombreEstado,Prestamo.idEstado AS Estado , Prestamo.idPrestamo,Prestamo.idEncargado,Cliente.idCliente,cantidad,codigoSecc,hora,fecha, Encargado.nombre AS encargado FROM Prestamo INNER JOIN Encargado ON Prestamo.idEncargado=Encargado.idEncargado INNER JOIN Estado ON Prestamo.idEstado=Estado.idEstado INNER JOIN Cliente ON Prestamo.idCliente=Cliente.idCliente");
			if(resultado!=null){
				listaPrestamo.clear();
				try{
					while(resultado.next()){
						Prestamo prestamo;
						//if(resultado.getString("codigoSecc").trim()!=""){
							if(veFecha.fechaPrestamo(resultado.getString("fecha"))==false){
								if(3==resultado.getInt("Estado")){
									prestamo=new Prestamo(resultado.getInt("idPrestamo"),resultado.getInt("idEncargado"),resultado.getInt("idCliente"),resultado.getInt("cantidad"), resultado.getString("codigoSecc"), resultado.getString("hora"), resultado.getString("fecha"),resultado.getString("encargado"),1,"Fuera de Fecha",resultado.getString("Cliente"));
								}else{
									prestamo=new Prestamo(resultado.getInt("idPrestamo"),resultado.getInt("idEncargado"),resultado.getInt("idCliente"),resultado.getInt("cantidad"), resultado.getString("codigoSecc"), resultado.getString("hora"), resultado.getString("fecha"),resultado.getString("encargado"),resultado.getInt("Estado"),"Entregado",resultado.getString("Cliente"));
								}
							}else{
								prestamo=new Prestamo(resultado.getInt("idPrestamo"),resultado.getInt("idEncargado"),resultado.getInt("idCliente"),resultado.getInt("cantidad"), resultado.getString("codigoSecc"), resultado.getString("hora"), resultado.getString("fecha"),resultado.getString("encargado"),resultado.getInt("Estado"),resultado.getString("NombreEstado"),resultado.getString("Cliente"));
							}
						//}else{
						//	prestamo=new Prestamo();
						//}
						listaPrestamo.add(prestamo);
					}
				}catch(SQLException sql){
					sql.printStackTrace();
				}
			}
		}		
	}
	/**
		Obtiene el ultimo idPrestamo
		@return idPrestamo
	*/
	public int getLastId(){
		int idPrestamo=0;
		if(usuarioConectado!=null){
			ResultSet resultado=cnx.ejecutarConsulta("EXEC UltimoIdPrestamo");
			if(resultado!=null){
				try{
					if(resultado.next()){
						idPrestamo=resultado.getInt("Maximo");
					}
				}catch(SQLException sqlI){
					sqlI.printStackTrace();
				}
			}
		}
		System.out.println(idPrestamo);
		return idPrestamo;
	}
	/**
		Obtiene la lista de prestamos
		@return listaPrestamo
	*/
	public ObservableList<Prestamo> getListaPrestamo(){
		actualizarPrestamo();
		return listaPrestamo;
	}
	/**
		Agrega un prestamo
		@param prestamo
	*/	
	public void agregarPrestamo(Prestamo prestamo){
		Calendar calendario = new GregorianCalendar();
		int hora =calendario.get(Calendar.HOUR_OF_DAY);
		int minutos = calendario.get(Calendar.MINUTE);
		String horaSistem=""+hora+":"+minutos;
		SimpleDateFormat formatFecha = new SimpleDateFormat("dd-mm-yyyy");
		Date fechaD=null;
		try{
			cnx.ejecutarSentencia("INSERT INTO Prestamo(idEncargado,idCliente,cantidad,codigoSecc,hora,fecha,idEstado) VALUES ("+prestamo.getIdEncargado()+", "+prestamo.getIdCliente()+", "+prestamo.getCantidad()+", '"+prestamo.getCodigoSecc()+"', '"+horaSistem+"', '"+prestamo.getFecha()+"', "+prestamo.getIdEstado()+")");
			actualizarPrestamo();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
		Elimina un prestamo
	*/
	public void eliminarPrestamo(){
		cnx.ejecutarSentencia("EXEC eliminarPrestamoIncorrecto");
		actualizarPrestamo();
	}
	/**
		Actualiza una herramienta
		@param campo
		@param valor
		@param idPrestamo
	*/
	public void actualizarPrestamos(String campo,String valor,int idPrestamo){
		if(!campo.equals("idEstado")){
			Integer.parseInt(valor);
			cnx.ejecutarSentencia("UPDATE Prestamo SET "+campo+"='"+valor+"' WHERE idPrestamo="+idPrestamo);
		}else{
			cnx.ejecutarSentencia("UPDATE Prestamo SET "+campo+"="+valor+" WHERE idPrestamo="+idPrestamo);
		}
		actualizarPrestamo();
	}
}
