package org.jarriaza.utilidad;


import javafx.scene.layout.GridPane;

import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.control.CheckBox;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.Event;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;

import java.util.ArrayList;
import java.util.Date;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import org.jarriaza.manejadores.ManejadorHerramienta;
import org.jarriaza.manejadores.ManejadorPrestamo;
import org.jarriaza.manejadores.ManejadorUsuario;
import org.jarriaza.manejadores.ManejadorCliente;
import org.jarriaza.manejadores.ManejadorDetallePrestamo;
import org.jarriaza.manejadores.ManejadorEncargado;
import org.jarriaza.beans.Herramienta;
import org.jarriaza.beans.Prestamo;
import org.jarriaza.beans.Encargado;
import org.jarriaza.beans.DetallePrestamo;
import org.jarriaza.beans.Cliente;
import org.jarriaza.beans.Usuario;
import org.jarriaza.db.Conexion;
import org.jarriaza.ui.Principal;
import org.jarriaza.beans.Objeto;
import org.jarriaza.utilidad.VerificarFecha;
/**
	Clase que agrega un Tab
*/
public class AgregarTab implements EventHandler<Event>{
	private Tab retornarTab;
	private SplitMenuButton sPanelHrr,sPanelProf;
	private GridPane gPanel,gPanelPrestamo;
	private ScrollPane scroll;
	private Button btnAgregarHerramienta,btnAgregarPrestamo,btnAgregarOtroPrestamo,btnAgregarUsuario,btnAgregarCliente,btnEliminarHer;
	private TextField txtNombre, txtCantidad,txtCantidad2,txtCodigoSecc,txtFecha,txtNombre2,txtUsername,txtNombre3;
	private PasswordField pfPassw,pfPassw2;
	private Label lblNombre, lblCantidad, lblErr, lblCliente, lblCodigoSecc,lblFecha, lblHerramienta,lblUsername,lblPassw,lblRango,lblPassw2;
	private ManejadorHerramienta mHerramienta;
	private ManejadorPrestamo mPrestamo;
	private ManejadorCliente mCliente;
	private ManejadorDetallePrestamo mDetallePrestamo;
	private ManejadorUsuario mUsuario;
	private ManejadorEncargado mEncargado;
	private int idPrestamo;
	private ArrayList<Herramienta> listaHerramientas;
	private ArrayList<Objeto> listadeObjetos;
	private TabPane tpPrincipal;
	private Conexion cnx;
	private SimpleDateFormat sDFHora;
	private VBox vBoxPanelH;
	private boolean aceptado;
	private CheckBox chkCRUD,chkPRESTAMISTA,chkVISOR;
	/**
		Agrega un tab segun sea el caso
		@param menuItem
		@param mUser
		@param cnx
		@param mPrestadoRecibidio recibe un manejador prestamo para actualizar la lista
	*/
	public Tab agregar(MenuItem menuItem,ManejadorUsuario mUser,Conexion cnxo,ManejadorPrestamo mPrestadoRecibidio){
		mPrestamo=mPrestadoRecibidio;
		aceptado=false;
		cnx=cnxo;
		mHerramienta=new ManejadorHerramienta(cnx);
		mUsuario=mUser;
		mHerramienta.setUsuarioConectado(mUsuario.getUsuarioAutenticado());
		retornarTab=new Tab();
		gPanel=new GridPane();
		scroll=new ScrollPane();
		lblErr=new Label();
		vBoxPanelH=new VBox();
		listadeObjetos=new ArrayList<Objeto>();
		if(menuItem.getId()=="1"){
			btnAgregarHerramienta=new Button("Agregar");
			btnAgregarHerramienta.addEventHandler(ActionEvent.ACTION, this);
			lblNombre = new Label("Herramienta");
			lblCantidad = new Label("Cantidad");
			txtNombre=new TextField();
			txtNombre.setPrefWidth(150);
			txtNombre.addEventHandler(KeyEvent.KEY_RELEASED, this);
			txtCantidad=new TextField();
			txtCantidad.setPrefWidth(150);
			txtCantidad.addEventHandler(KeyEvent.KEY_RELEASED, this);
			gPanel.add(lblNombre,0,0);
			gPanel.add(txtNombre,1,0);
			gPanel.add(lblCantidad,0,1);
			gPanel.add(txtCantidad,1,1);
			gPanel.add(btnAgregarHerramienta,1,2);
			gPanel.setVgap(2);
			gPanel.setHgap(5);
			retornarTab.setContent(gPanel);
			retornarTab.setText("Agregar Herramienta");
		}else if(menuItem.getId()=="2"){
			btnEliminarHer=new Button("Eliminar herramienta");
			btnEliminarHer.addEventHandler(ActionEvent.ACTION,this);
			mCliente=new ManejadorCliente(cnx);
			mCliente.setUsuarioConectado(mUsuario.getUsuarioAutenticado());
			mPrestamo=mPrestadoRecibidio;
			btnAgregarPrestamo=new Button("Agregar");
			btnAgregarPrestamo.addEventHandler(ActionEvent.ACTION, this);
			lblCliente=new Label("Profesor:");
			sPanelProf= new SplitMenuButton();
			sPanelProf.setText("Profesor");
			sPanelProf.setPrefWidth(150);
			ObservableList<Cliente> listaCliente=FXCollections.observableArrayList(mCliente.getlistaClientes());
			for(Cliente profesor:listaCliente){
				MenuItem propiedad=new MenuItem(""+profesor.getNombre());
				propiedad.setOnAction(new EventHandler<ActionEvent>() {
					@Override public void handle(ActionEvent e) {
						sPanelProf.setText(propiedad.getText());
					}
				});
				sPanelProf.getItems().add(propiedad);
				
			}
			txtCantidad2=new TextField("");
			txtCantidad2.addEventHandler(KeyEvent.KEY_RELEASED, this);
			lblCantidad= new Label("Cantidad:");
			txtCodigoSecc=new TextField("");
			txtCodigoSecc.addEventHandler(KeyEvent.KEY_RELEASED, this);
			lblCodigoSecc=new Label("Codigo:");
			txtFecha=new TextField("");
			txtFecha.addEventHandler(KeyEvent.KEY_RELEASED, this);
			lblFecha=new Label("Fecha Entrega:");
			btnAgregarOtroPrestamo=new Button("Agregar otra herramienta");
			btnAgregarOtroPrestamo.addEventHandler(ActionEvent.ACTION,this);
			lblHerramienta=new Label("Herramienta:");
			ObservableList<Herramienta> listaHerramientasO=FXCollections.observableArrayList(mHerramienta.getListaHerramientas());
			//listaHerramientas=new ArrayList<Herramienta>(mHerramienta.getListaHerramientas());
			sPanelHrr= new SplitMenuButton();
			sPanelHrr.setText("Herramienta");
			sPanelHrr.setPrefWidth(150);
			for(Herramienta herramienta:listaHerramientasO){
				MenuItem propiedad=new MenuItem(""+herramienta.getNombre());
				propiedad.setOnAction(new EventHandler<ActionEvent>() {
					@Override public void handle(ActionEvent e) {
						sPanelHrr.setText(propiedad.getText());
					}
				});
				sPanelHrr.getItems().add(propiedad);
				
			}		
			listadeObjetos.add(new Objeto(sPanelHrr.getText(),txtCantidad2,sPanelHrr));				
			gPanelPrestamo=new GridPane();
			gPanelPrestamo.add(lblCliente,0,0);
			gPanelPrestamo.add(sPanelProf,1,0);
			gPanelPrestamo.add(lblCodigoSecc,2,0);
			gPanelPrestamo.add(txtCodigoSecc,3,0);
			gPanelPrestamo.add(lblFecha,0,1);
			gPanelPrestamo.add(txtFecha,1,1);						
			gPanelPrestamo.add(lblHerramienta,0,2);
			gPanelPrestamo.add(sPanelHrr,1,2);
			gPanelPrestamo.add(lblCantidad,2,2);
			gPanelPrestamo.add(txtCantidad2,3,2);
			gPanelPrestamo.add(btnAgregarOtroPrestamo,3,3);
			gPanelPrestamo.add(btnAgregarPrestamo,2,4);
			gPanelPrestamo.setHgap(8);
			gPanelPrestamo.setVgap(8);
			scroll.setContent(gPanelPrestamo);
			retornarTab.setContent(scroll);
			retornarTab.setText("Agregar Prestamo");
		}else if(menuItem.getId()=="3"){
			
		}else if(menuItem.getId()=="4"){
			gPanelPrestamo=new GridPane();
			btnAgregarUsuario=new Button("Agregar");
			btnAgregarUsuario.addEventHandler(ActionEvent.ACTION,this);
			lblNombre=new Label("Nombre: ");
			lblUsername=new Label("Username: ");
			lblPassw=new Label("Password: ");
			lblPassw2=new Label("Repite Password:");
			txtNombre2=new TextField("");
			txtNombre2.addEventHandler(KeyEvent.KEY_RELEASED, this);
			txtUsername=new TextField("");			
			pfPassw=new PasswordField();
			pfPassw.addEventHandler(KeyEvent.KEY_RELEASED, this);
			pfPassw2=new PasswordField();
			pfPassw2.addEventHandler(KeyEvent.KEY_RELEASED, this);
			chkCRUD=new CheckBox("CRUD");
			chkPRESTAMISTA=new CheckBox("PRESTAMISTA");
			chkVISOR=new CheckBox("VISOR");
			gPanelPrestamo.add(lblNombre,0,0);
			gPanelPrestamo.add(txtNombre2,1,0);
			gPanelPrestamo.add(lblUsername,0,1);
			gPanelPrestamo.add(txtUsername,1,1);
			gPanelPrestamo.add(lblPassw,0,2);
			gPanelPrestamo.add(pfPassw,1,2);
			gPanelPrestamo.add(lblPassw2,0,3);
			gPanelPrestamo.add(pfPassw2,1,3);
			gPanelPrestamo.add(chkCRUD,0,4);
			gPanelPrestamo.add(chkPRESTAMISTA,1,4);
			gPanelPrestamo.add(chkVISOR,2,4);
			gPanelPrestamo.add(btnAgregarUsuario,2,5);
			gPanelPrestamo.setHgap(5);
			gPanelPrestamo.setVgap(5);
			retornarTab.setContent(gPanelPrestamo);
			retornarTab.setText("Agregar Usuario");
		}else if(menuItem.getId()=="5"){
			gPanelPrestamo=new GridPane();
			lblNombre=new Label("Nombre: ");
			txtNombre3=new TextField("");
			txtNombre3.addEventHandler(KeyEvent.KEY_RELEASED,this);
			btnAgregarCliente= new Button("Agregar");
			btnAgregarCliente.addEventHandler(ActionEvent.ACTION,this);
			gPanelPrestamo.add(lblNombre,0,0);
			gPanelPrestamo.add(txtNombre3,1,0);
			gPanelPrestamo.add(btnAgregarCliente,1,1);
			gPanelPrestamo.setHgap(5);
			gPanelPrestamo.setVgap(5);
			retornarTab.setContent(gPanelPrestamo);
			retornarTab.setText("Agregar Profesor");
		}
		return retornarTab;
	}
	/**
		Metodo de eventos
		@param event
	*/
	public void handle(Event event){
		if(event instanceof KeyEvent){
			if(((KeyEvent)event).getCode() == KeyCode.ENTER){
				if(event.getSource()==txtNombre||event.getSource()==txtCantidad){	
					agregarHerramienta();
				}else if(event.getSource()==txtCodigoSecc||event.getSource()==txtFecha||event.getSource()==txtCantidad2){
					agregarPrestamo();
				}else if(event.getSource()==txtNombre2||event.getSource()==pfPassw||event.getSource()==pfPassw2||event.getSource()==txtUsername){
					agregarUsuario();
				}else if(event.getSource()==txtNombre3){
					agregarCliente();
				}
			}
		}else if(event instanceof ActionEvent){
			if(event.getSource()==btnAgregarHerramienta){
				agregarHerramienta();
			}else if(event.getSource()==btnAgregarPrestamo){
				agregarPrestamo();
			}else if(event.getSource()==btnAgregarUsuario){
				agregarUsuario();
			}else if(event.getSource()==btnAgregarCliente){
				agregarCliente();
			}else if(event.getSource()==btnEliminarHer){	
				try{
					try{						
						vBoxPanelH.getChildren().remove(listadeObjetos.size()-4);
						vBoxPanelH.getChildren().add(btnAgregarOtroPrestamo);
						vBoxPanelH.getChildren().add(btnAgregarPrestamo);
						vBoxPanelH.getChildren().add(btnEliminarHer);	
					}catch(ArrayIndexOutOfBoundsException arr){
						vBoxPanelH.getChildren().remove(btnEliminarHer);
					}							
				}catch(IllegalArgumentException ee){

				}	
			}else if(event.getSource()==btnAgregarOtroPrestamo){
				Label lblHerramienta2=new Label("Herramienta:");
				SplitMenuButton sPanelHrr2= new SplitMenuButton();
				sPanelHrr2.setText("Herramienta ");
				sPanelHrr2.setPrefWidth(150);
				ObservableList<Herramienta> listaHerramientasO=FXCollections.observableArrayList(mHerramienta.getListaHerramientas());
				for(Herramienta herramienta:listaHerramientasO){
					MenuItem propiedad=new MenuItem(""+herramienta.getNombre());
					propiedad.setOnAction(new EventHandler<ActionEvent>() {
						@Override public void handle(ActionEvent e) {
							sPanelHrr2.setText(propiedad.getText());
						}
					});
					sPanelHrr2.getItems().add(propiedad);

				}	
				Label lblCantidad2=new Label("Cantidad:");
				TextField txtCantidad3=new TextField("");
				listadeObjetos.add(new Objeto(sPanelHrr2.getText(),txtCantidad3,sPanelHrr2));
				HBox hBoxPanel=new HBox();
				vBoxPanelH.getChildren().remove(btnAgregarOtroPrestamo);
				vBoxPanelH.getChildren().remove(btnAgregarPrestamo);
				vBoxPanelH.getChildren().remove(btnEliminarHer);	
				hBoxPanel.getChildren().add(lblHerramienta2);
				hBoxPanel.getChildren().add(sPanelHrr2);
				hBoxPanel.getChildren().add(lblCantidad2);
				hBoxPanel.getChildren().add(txtCantidad3);
				hBoxPanel.setSpacing(13);
				gPanelPrestamo.getChildren().remove(btnAgregarOtroPrestamo);
				gPanelPrestamo.getChildren().remove(btnAgregarPrestamo);
				gPanelPrestamo.getChildren().remove(btnEliminarHer);				
				try{
					vBoxPanelH.getChildren().add(hBoxPanel);
					vBoxPanelH.getChildren().add(btnAgregarOtroPrestamo);
					vBoxPanelH.getChildren().add(btnAgregarPrestamo);
					vBoxPanelH.getChildren().add(btnEliminarHer);
					vBoxPanelH.setSpacing(8);
					gPanelPrestamo.add(vBoxPanelH,0,3,4,10);
					txtFecha.setPrefWidth(75);					
				}catch(IllegalArgumentException e){

				}
				
			}
		}
	}
	/**
		Verifica que los datos no sean nulos
	*/
	private boolean verificarDatos(){
		return !txtNombre.getText().trim().equals("") & !txtCantidad.getText().trim().equals("");
	}
	/**
		Verifica que los datos no sean nulos
	*/
	private boolean verificarCampos(){
		boolean verificar=false;
		for(int cont=0;cont<listadeObjetos.size();cont++){
			Objeto objeto=listadeObjetos.get(cont);
			if(!objeto.getCantidad().getText().trim().equals("") && !objeto.getSMenu().getText().equals("Herramienta") && !sPanelProf.getText().equals("Profesor") && !txtCantidad2.getText().trim().equals("") && !txtFecha.getText().trim().equals("") && !txtCodigoSecc.getText().trim().equals("")){
				verificar=true;
			}else{
				verificar=false;
				return false;
			}
		}
		return verificar;
	}
	/**
		Verifica las cantidades de los artículos
		@return verificar Regresa un 1 si las cantidades son correctas y el nombre del articulo si no
	*/
	public String verificarCantidad(ArrayList<String> idHerramientas, ArrayList<String> cantHerramientas){
		String verificar="1";
		for(int contar=0;contar<idHerramientas.size();contar++){
			int cantidadPrestar=Integer.parseInt(listadeObjetos.get(contar).getCantidad().getText());							
			int cantidadBodega=Integer.parseInt(cantHerramientas.get(contar));
			if(cantidadBodega>=cantidadPrestar && cantidadBodega>0 && cantidadPrestar>0){
				verificar="1";
			}else{
				verificar=listadeObjetos.get(contar).getSMenu().getText();
				return verificar;
			}
		}
		return verificar;
	}


	/**
		Agrega un cliente o profesor a la db
	*/
	public void agregarCliente(){
		gPanelPrestamo.getChildren().remove(lblErr);
		try{
			if(!txtNombre3.getText().trim().equals("")){
				mCliente=new ManejadorCliente(cnx);
				mCliente.setUsuarioConectado(mUsuario.getUsuarioAutenticado());
				mCliente.agregarCliente(new Cliente(0,txtNombre3.getText()));
				mPrestamo.actualizarPrestamo();
				lblErr.setText("Profesor agreagdo correctamente..");	
			}else{
				lblErr.setText("Falta rellenar algun dato.");	
			}
		}catch(IllegalArgumentException iaea){

		}
		gPanelPrestamo.add(lblErr,1,2,2,1);
	}
	/**
		Verifica que los campos para agregar un usuario no sean nulos
	*/
	public boolean verificarCamposUsuario(){
		if(!txtNombre2.getText().trim().equals("") && !txtUsername.getText().trim().equals("") &&!pfPassw.getText().trim().equals("") && !pfPassw2.getText().trim().equals("")){
			if(chkCRUD.isSelected()==true||chkPRESTAMISTA.isSelected()==true||chkVISOR.isSelected()==true){
				return true;
			}else{
				return false;
			}
		}
		return false;
	}
	/**
		Agrega un usuario a la db, con sus respectivos modulos
	*/
	public void agregarUsuario(){
		gPanelPrestamo.getChildren().remove(lblErr);
		try{
			if(verificarCamposUsuario()){
				if(pfPassw.getText().equals(pfPassw2.getText())){
					ArrayList<Usuario> listaUsuario=new ArrayList<Usuario>(mUsuario.getListaUsuarios());
					boolean username=false;
					for(Usuario user:listaUsuario){
						if(user.getUsername().equalsIgnoreCase(txtUsername.getText())){
							username=true;
						}
					}		
					if(username==false){
						mEncargado=new ManejadorEncargado(cnx);
						Encargado encargado=new Encargado(0,txtNombre2.getText());
						mEncargado.agregarEncargado(encargado);
						Usuario usuarioAgregar=new Usuario();
						usuarioAgregar.setUsername(txtUsername.getText());
						usuarioAgregar.setPassw(pfPassw.getText());
						usuarioAgregar.setNombre(txtNombre2.getText());
						if(chkCRUD.isSelected()==true && chkPRESTAMISTA.isSelected()==false && chkVISOR.isSelected()==false){
							usuarioAgregar.setRango(2);
						}else if(chkPRESTAMISTA.isSelected()==true && chkCRUD.isSelected()==false && chkVISOR.isSelected()==false){
							usuarioAgregar.setRango(3);
						}else if(chkVISOR.isSelected()==true && chkCRUD.isSelected()==false && chkPRESTAMISTA.isSelected()==false){
							usuarioAgregar.setRango(4);
						}else if(chkCRUD.isSelected()==true && chkPRESTAMISTA.isSelected()==true && chkVISOR.isSelected()==false){
							usuarioAgregar.setRango(5);
						}else if(chkCRUD.isSelected()==true && chkVISOR.isSelected()==true && chkPRESTAMISTA.isSelected()==false){
							usuarioAgregar.setRango(6);
						}else if(chkPRESTAMISTA.isSelected()==true && chkVISOR.isSelected()==true && chkCRUD.isSelected()==false){
							usuarioAgregar.setRango(7);
						}else if(chkCRUD.isSelected()==true && chkPRESTAMISTA.isSelected()==true && chkVISOR.isSelected()==true){
							usuarioAgregar.setRango(1);
						}
						mUsuario.agregarUsuario(usuarioAgregar);
						lblErr.setText("Usuario agregado correctamente.");					
					}else{
						lblErr.setText("Ya hay alguien registrado con ese username.");					
					}
				}else{
					lblErr.setText("La password no es correcta.");					
				}
			}else{
				lblErr.setText("Falta rellenar algun dato.");				
			}
			gPanelPrestamo.add(lblErr,6,2,2,1);
		}catch(IllegalArgumentException iea){
		
		}
	}
	/**
		Agrega un prestamo a la db
	*/
	public void agregarPrestamo(){
		gPanelPrestamo.getChildren().remove(lblErr);		
		try{
			if(verificarCampos()){
				if(new VerificarFecha().fechaPrestamo(txtFecha.getText())){
					try{
						int idC=0;
						ArrayList<Cliente> listC= new ArrayList(mCliente.getlistaClientes());
						ArrayList<Herramienta> listH= new ArrayList(mHerramienta.getListaHerramientas());
						ArrayList<String> idHerramientas= new ArrayList();
						ArrayList<String> cantHerramientas= new ArrayList(); 
						for(Cliente cliente:listC){
							if(cliente.getNombre().equalsIgnoreCase(sPanelProf.getText())){
								idC=cliente.getIdCliente();							
							}
						}
						for(Objeto objeto:listadeObjetos){
							for(Herramienta herramienta:listH){
								if(herramienta.getNombre().equalsIgnoreCase(objeto.getSMenu().getText())){
									idHerramientas.add(""+herramienta.getIdHerramienta());
									cantHerramientas.add(""+herramienta.getCantidad());
								}
							}
						}
						//int idVerificacion=mVerificacion.getLastId()+1;
						boolean hecho=false;					
						for(int contar=0;contar<idHerramientas.size();contar++){
							SimpleDateFormat formatFecha = new SimpleDateFormat("dd-mm-yyyy");
							Date dateFecha=null;
							int cantidadPrestar=Integer.parseInt(listadeObjetos.get(contar).getCantidad().getText());							
							int cantidadBodega=Integer.parseInt(cantHerramientas.get(contar));					
							try{							
								String value=verificarCantidad(idHerramientas,cantHerramientas);
								if(value=="1"){		
									if(contar==0){
										dateFecha = formatFecha.parse(txtFecha.getText());
										Prestamo prestamo=new Prestamo();
										prestamo.setIdCliente(idC);
										prestamo.setIdEncargado(mUsuario.getUsuarioAutenticado().getIdEncargado());
										prestamo.setCodigoSecc(txtCodigoSecc.getText());									
										prestamo.setFecha(txtFecha.getText());
										prestamo.setCantidad(0);
										prestamo.setHora("12:45");
										prestamo.setIdEstado(1);
										mPrestamo.agregarPrestamo(prestamo);	
										idPrestamo=mPrestamo.getLastId();
									}	
									try{
										mDetallePrestamo=new ManejadorDetallePrestamo(cnx);
										mDetallePrestamo.setUsuarioConectado(mUsuario.getUsuarioAutenticado(),0,0);
										DetallePrestamo detallePrestamo=new DetallePrestamo();
										detallePrestamo.setIdPrestamo(idPrestamo);
										detallePrestamo.setIdHerramienta(Integer.parseInt(idHerramientas.get(contar)));
										detallePrestamo.setCantidad(Integer.parseInt(listadeObjetos.get(contar).getCantidad().getText()));
										mDetallePrestamo.agregarDetallePrestamo(detallePrestamo);	
										lblErr.setText("Prestamo agregado correctamente.");
									}catch(Exception sqlE){
										lblErr.setText("No puedes repetir este articulo.");
										mPrestamo.eliminarPrestamo();
									}														
									try{
										gPanelPrestamo.add(lblErr,4,4,2,1);	
									}catch(IllegalArgumentException e){
									
									}
									aceptado=true;
									/*mDetallePrestamo=new ManejadorVerificacion(cnx);
									mDetallePrestamo.setUsuarioConectado(mUsuario.getUsuarioAutenticado(),0,0);
									DetallePrestamo detallePrestamo=new DetallePrestamo();
									detallePrestamo.setIdPrestamo(idPrestamo);
									detallePrestamo.setIdHerramienta(Integer.parseInt(idHerramientas.get(contar)));
									detallePrestamo.setCantidad(Integer.parseInt(listadeObjetos.get(contar).getCantidad().getText()));
									mDetallePrestamo.agregarDetallePrestamo(detallePrestamo);	
									lblErr.setText("Solo falta que el prestamo sea verificado.");
									gPanelPrestamo.add(lblErr,4,4,2,1);	
									aceptado=true;*/
								}else{
									lblErr.setText("No hay tal cantidad para este articulo "+value+".");
									gPanelPrestamo.add(lblErr,4,4,2,1);
								}
							}catch(ParseException e){
								e.printStackTrace();
								lblErr.setText("La fecha debe ser dd-mm-yyyy");
								gPanelPrestamo.add(lblErr,4,4,2,1);
							}
						}			
					}catch(NumberFormatException nfe){
						lblErr.setText("La cantidad debe ser numerica.");
						gPanelPrestamo.add(lblErr,4,4,2,1);
					}
				}else{
					lblErr.setText("Ingresa una fecha valida.");
					gPanelPrestamo.add(lblErr,4,4,2,1);
				}
			}else{
				lblErr.setText("Falta rellenar algun dato.");
				gPanelPrestamo.add(lblErr,4,4,2,1);
			}
		}catch(IllegalArgumentException childDup){

		}
	}
	/**
		Agrega una herramienta a la db
	*/
	public void agregarHerramienta(){
		gPanel.getChildren().remove(lblErr);
		boolean verificar=false;
		try{
			if(verificarDatos()){
				ObservableList<Herramienta> listaHerramientasO=FXCollections.observableArrayList(mHerramienta.getListaHerramientas());
				//listaHerramientas = new ArrayList<Herramienta>(mHerramienta.getListaHerramientas());
				for(Herramienta herramientaBuscar:listaHerramientasO){
					if(herramientaBuscar.getNombre().equalsIgnoreCase(txtNombre.getText())){						
						lblErr.setText("Ya hay una herramienta con ese nombre.");
						gPanel.add(lblErr,1,3,2,1);						
						verificar=false;
					}else{						
						verificar=true;
					}
				}
				if(verificar==true){
					try{
						gPanel.getChildren().remove(lblErr);
						Herramienta herramienta=new Herramienta(0,txtNombre.getText(),Integer.parseInt(txtCantidad.getText()));
						mHerramienta.agregarHerramienta(herramienta);
						mPrestamo.actualizarPrestamo();
						lblErr.setText("Herramienta agregada correctamente.");
						aceptado=true;
						gPanel.add(lblErr,1,3,2,1);
					}catch(NumberFormatException e){
						lblErr.setText("La cantidad debe ser numerica.");
						gPanel.add(lblErr,1,3,2,1);
					}
				}
			}else{
				lblErr.setText("Falta rellenar algun dato.");
				gPanel.add(lblErr,1,3,2,1);
			}
		}catch(IllegalArgumentException childDup){

		}
	}
	/**
		Obtiene si la ventada ya fue agregada
		@return aceptado Si fue creada la consulta
	*/
	public boolean getAceptado(){
		return aceptado;
	}
}