package org.jarriaza.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
/**
	Beans prestamo
*/
public class Prestamo{
	private StringProperty codigoSecc , hora , fecha, encargado, nombreEstado, profesor;
	private IntegerProperty idPrestamo,idEncargado,idCliente,cantidad,idEstado;
	/**
		Constructor vacio
	*/
	public Prestamo(){
		this.init();
	}
	/**
		Constructor con parametros
		@param idPrestamo
		@param idEncargado
		@param idCliente
		@param cantidad
		@param codgigoSecc
		@param hora
		@param fecha
		@param encargado
		@param idEstado
		@param nombreEstado
		@param profesor
	*/
	public Prestamo(int idPrestamo , int idEncargado, int idCliente, int cantidad, String codigoSecc,String hora, String fecha, String encargado, int idEstado, String nombreEstado, String profesor){
		this.init();
		this.setIdPrestamo(idPrestamo);	
		this.setIdEncargado(idEncargado);
		this.setIdCliente(idCliente);	
		this.setCantidad(cantidad);	
		this.setCodigoSecc(codigoSecc);	
		this.setHora(hora);
		this.setFecha(fecha);
		this.setEncargado(encargado);
		this.setIdEstado(idEstado);
		this.setNombreEstado(nombreEstado);
		this.setProfesor(profesor);
	}
	/**
		Inicializa las variables
	*/
	private void init(){
		this.idPrestamo = new SimpleIntegerProperty();
		this.idEncargado = new SimpleIntegerProperty();
		this.idCliente = new SimpleIntegerProperty();
		this.cantidad = new SimpleIntegerProperty();
		this.codigoSecc = new SimpleStringProperty();
		this.profesor = new SimpleStringProperty();
		this.hora = new SimpleStringProperty();
		this.fecha = new SimpleStringProperty();
		this.nombreEstado = new SimpleStringProperty();
		this.encargado= new SimpleStringProperty();
		this.idEstado = new SimpleIntegerProperty();		
	}
	/**
		Obtiene el codigoSecc
		@return codigoSecc
	*/
	public String getCodigoSecc(){
		return this.codigoSecc.get();
	}
	/**
		Ingresa el codigoSecc
		@param codigoSecc
	*/
	public void setCodigoSecc(String codigoSecc){
		this.codigoSecc.set(codigoSecc);
	}
	/**
		Obtiene el codigoSecc observable
		@return codigoSecc
	*/
	public StringProperty codigoSeccProperty(){
		return this.codigoSecc;
	}
	/**
		Obtiene el nombreEstado
		@return nombreEstado
	*/
	public String getNombreEstado(){
		return this.nombreEstado.get();
	}
	/**
		Ingresa el nombreEstado
		@param nombreEstado
	*/
	public void setNombreEstado(String nombreEstado){
		this.nombreEstado.set(nombreEstado);
	}
	/**
		Obtiene el nombreEstado observable
		@return nombreEstado
	*/
	public StringProperty nombreEstadoProperty(){
		return this.nombreEstado;
	}
	/**
		Obtiene el profesor
		@return profesor
	*/
	public String getProfesor(){
		return this.profesor.get();
	}
	/**
		Ingresa el profesor
		@param profesor
	*/
	public void setProfesor(String profesor){
		this.profesor.set(profesor);
	}
	/**
		Obtiene el profesor observable
		@return profesor
	*/
	public StringProperty profesorProperty(){
		return this.profesor;
	}
	
	/**
		Obtiene el encargado
		@return encargado
	*/
	public String getEncargado(){
		return this.encargado.get();
	}
	/**
		Ingresa el encargado
		@param encargado
	*/
	public void setEncargado(String encargado){
		this.encargado.set(encargado);
	}
	/**
		Obtiene el encargado observable
		@return encargado
	*/
	public StringProperty encargadoProperty(){
		return this.encargado;
	}
	/**
		Obtiene la hora
		@return hora
	*/
	public String getHora(){
		return this.hora.get();
	}
	/**
		Ingresa la hora
		@param hora
	*/
	public void setHora(String hora){
		this.hora.set(hora);
	}
	/**
		Obtiene la hora observable
		@return hora
	*/
	public StringProperty horaProperty(){
		return this.hora;
	}
	/**
		Obtiene la fecha
		@return fecha
	*/
	public String getFecha(){
		return this.fecha.get();
	}
	/**
		Ingresa la fecha
		@param fecha
	*/
	public void setFecha(String fecha){
		this.fecha.set(fecha);
	}
	/**
		Obtiene la fecha observable
		@return fecha
	*/
	public StringProperty fechaProperty(){
		return this.fecha;
	}
	/**
		Obtiene el idEncargado
		@return idEncargado
	*/
	public int getIdEncargado(){
		return this.idEncargado.get();
	}
	/**
		Ingresa el idEncargado
		@param idEncargado
	*/
	public void setIdEncargado(int idEncargado){
		this.idEncargado.set(idEncargado);
	}
	/**
		Obtiene el idEncargado observable
		@return idEncargado
	*/
	public IntegerProperty idEncargadoProperty(){
		return this.idEncargado;
	}
	
	/**
		Obtiene el idPrestamo
		@return idPrestamo
	*/
	public int getIdPrestamo(){
		return this.idPrestamo.get();
	}
	/**
		Ingresa el idPrestamo
		@param idPrestamo
	*/
	public void setIdPrestamo(int id){
		this.idPrestamo.set(id);
	}
	/**
		Obtiene el idPrestamo observable
		@return idPrestamo
	*/
	public IntegerProperty idPrestamoProperty(){
		return this.idPrestamo;
	}
	/**
		Obtiene el idCliente
		@return idCliente
	*/
	public int getIdCliente(){
		return this.idCliente.get();
	}
	/**
		Ingresa el idCliente
		@param idCliente
	*/
	public void setIdCliente(int id){
		this.idCliente.set(id);
	}
	/**
		Obtiene el idCliente observable
		@return idCliente
	*/
	public IntegerProperty idClienteProperty(){
		return this.idCliente;
	}
	/**
		Obtiene la cantidad
		@return cantidad
	*/
	public int getCantidad(){
		return this.cantidad.get();
	}
	/**
		Ingresa la cantidad
		@param cantidad
	*/
	public void setCantidad(int cantidad){
		this.cantidad.set(cantidad);
	}
	/**
		Obtiene la cantidad observable
		@return cantidad
	*/
	public IntegerProperty cantidadProperty(){
		return this.cantidad;
	}
	/**
		Obtiene el idEstado
		@return idEstado
	*/
	public int getIdEstado(){
		return this.idEstado.get();
	}
	/**
		Ingresa el idEstado
		@param idEstado
	*/
	public void setIdEstado(int idEstado){
		this.idEstado.set(idEstado);
	}
	/**
		Obtiene el idEstado observable
		@return idEstado
	*/
	public IntegerProperty idEstadoProperty(){
		return this.idEstado;
	}
}
