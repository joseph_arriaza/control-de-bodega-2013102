package org.jarriaza.manejadores;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import java.sql.SQLException;
import java.sql.ResultSet;

import org.jarriaza.beans.DetallePrestamo;
import org.jarriaza.beans.Usuario;
import org.jarriaza.db.Conexion;
import org.jarriaza.manejadores.ManejadorCliente;
import org.jarriaza.manejadores.ManejadorPrestamo;
/**
	Manejador del beans detalle prestamo
*/
public class ManejadorDetallePrestamo{
	private ObservableList<DetallePrestamo> listaDetallePrestamo;
	private ManejadorPrestamo mPrestamo;
	private Conexion cnx;
	private Usuario usuarioConectado;
	private int idCliente;
	private int idPrestamo;
	/**
		Instancia la lista
		@param cnx
	*/
	public ManejadorDetallePrestamo(Conexion cnx){
		this.listaDetallePrestamo = FXCollections.observableArrayList();
		this.cnx = cnx;
	}
	/**
		Ingresa el usuario conectado
		@param usuarioConectado
		@param idCliente
	*/
	public void setUsuarioConectado(Usuario usuarioConectado, int idCliente, int idPrestamo){
		this.usuarioConectado=usuarioConectado;
		this.idCliente=idCliente;
		this.idPrestamo=idPrestamo;
		actualizarDetallePrestamo(idCliente,idPrestamo);
	}
	/**
		Obtiene el usuario conectado
		@return usuarioConectado
	*/
	public Usuario getUsuarioConectado(){
		return this.usuarioConectado;
	}
	/**
		Actualiza la lista de prestamos segun sea el cliente
		@param idCliente
	*/
	public void actualizarDetallePrestamo(int idCliente, int idPrestamo){
		if(usuarioConectado!=null){
			ResultSet resultado = cnx.ejecutarConsulta("EXEC verificarPrestamos "+idCliente+","+idPrestamo);
			if(resultado!=null){
				listaDetallePrestamo.clear();
				try{
					while(resultado.next()){
						DetallePrestamo detallePrestamo=new DetallePrestamo(resultado.getInt("idPrestamo"),resultado.getInt("idHerramienta"),resultado.getInt("Cantidad"),resultado.getString("Fecha"),resultado.getString("Hora"),resultado.getString("Herramienta"));
						listaDetallePrestamo.add(detallePrestamo);
					}
				}catch(SQLException sql){
					sql.printStackTrace();
				}
			}
		}		
	}
	/**
		Obtiene la lista de prestamos
		@return listaDetallePrestamo
	*/
	public ObservableList<DetallePrestamo> getListaDetallePrestamo(){
		return listaDetallePrestamo;
	}
	/**
		Agrega articulos al prestamo
		@param detallePrestamo
	*/
	public void agregarDetallePrestamo(DetallePrestamo detallePrestamo){
		cnx.ejecutarSentencia("INSERT INTO DetallePrestamo(idPrestamo,idHerramienta,cantidad) VALUES ("+detallePrestamo.getIdPrestamo()+","+detallePrestamo.getIdHerramienta()+", "+detallePrestamo.getCantidad()+")");
		actualizarDetallePrestamo(idCliente,idPrestamo);		
	}
	/**
		Elimina un detallePrestamo
		@param idPrestamo
	*/
	public void eliminarDetallePrestamo(int idPrestamo){
		cnx.ejecutarSentencia("DELETE FROM DetallePrestamo WHERE idPrestamo="+idPrestamo);
		actualizarDetallePrestamo(idCliente,idPrestamo);
	}
	/**
		Actualiza un prestamo
		@param campo
		@param valor
		@param idPrestamo
		@param idHerramienta
		@param idCliente
	*/
	public void actualizarDetallePrestamos(String campo,String valor,int idPrestamo, int idCliente, int idHerramienta){
		cnx.ejecutarSentencia("UPDATE DetallePrestamo SET "+campo+"='"+valor+"' WHERE idPrestamo="+idPrestamo+" AND idHerramienta="+idHerramienta+" AND idCliente="+idCliente);
		actualizarDetallePrestamo(idCliente,idPrestamo);
	}
}
