CREATE DATABASE dbBodegaK
GO
USE dbBodegaK
GO
CREATE TABLE Usuario(
	idUsuario INT IDENTITY(1,1) PRIMARY KEY,
	username VARCHAR(255) NOT NULL,
	passw VARCHAR(255) NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	rango NUMERIC(1) NOT NULL,
	idEncargado INT NULL
)
--rango 1, admin
--rango 2, modulo crud
--rango 3, modulo prestamista
--rango 4, modulo visor
--rango 5 modulo crud y prestamista
--rango 6 modulo crud y visor
--rango 7 modulo visor y prestamista
CREATE TABLE Estado(
	idEstado INT IDENTITY(1,1) PRIMARY KEY,
	nombre VARCHAR(255) NOT NULL
)
CREATE TABLE Encargado(
	idEncargado INT IDENTITY(1,1) PRIMARY KEY,
	nombre VARCHAR(255) NOT NULL	
)
GO
CREATE TABLE Cliente(
	idCliente INT IDENTITY(1,1) PRIMARY KEY,
	nombre VARCHAR(255) NOT NULL
)
--rango 1 Admin y el rango 2 usuario
GO
CREATE TABLE Herramienta(
	idHerramienta INT IDENTITY(1,1) PRIMARY KEY,
	nombre VARCHAR(255) NOT NULL,
	cantidad INT NOT NULL
)
GO
CREATE TABLE Prestamo(
	idPrestamo INT IDENTITY (1,1) PRIMARY KEY,
	idCliente INT NOT NULL,
	idEncargado INT NOT NULL,
	cantidad INT NOT NULL,
	codigoSecc VARCHAR (255) NOT NULL,
	hora VARCHAR (5) NOT NULL,
	fecha VARCHAR (255) NOT NULL,
	idEstado INT NOT NULL,
	FOREIGN KEY (idCliente) REFERENCES Cliente(idCliente),
	FOREIGN KEY (idEncargado) REFERENCES Encargado(idEncargado),
	FOREIGN KEY (idEstado) REFERENCES Estado(idEstado)
)
GO
CREATE TABLE DetallePrestamo(
	idPrestamo INT NOT NULL,
	idHerramienta INT NOT NULL,
	cantidad INT NOT NULL,
	PRIMARY KEY (idPrestamo,idHerramienta),
	FOREIGN KEY (idPrestamo) REFERENCES Prestamo(idPrestamo),
	FOREIGN KEY (idHerramienta) REFERENCES Herramienta(idHerramienta),
)
--estado 1 pendiente, estado 2 devuelto, estado 3 fuera de fecha
GO 
CREATE TABLE Devolucion(
	idDevolucion INT IDENTITY(1,1) PRIMARY KEY,
	idCliente INT NOT NULL,
	idPrestamo INT NOT NULL,
	fecha DATE NOT NULL,
	FOREIGN KEY (idCliente) REFERENCES Cliente(idCliente),
	FOREIGN KEY (idPrestamo) REFERENCES Prestamo(idPrestamo),
)
GO
CREATE TABLE DetalleDevolucion(
	idDevolucion INT NOT NULL,
	idHerramienta INT NOT NULL,
	cantidad INT NOT NULL,
	PRIMARY KEY (idDevolucion, idHerramienta),
	FOREIGN KEY (idDevolucion) REFERENCES Devolucion(idDevolucion),
	FOREIGN KEY (idHerramienta) REFERENCES Herramienta(idHerramienta),
)
GO
INSERT INTO Usuario(username,passw,nombre,rango,idEncargado) VALUES ('Juan','juan','juan',1,1)
INSERT INTO Usuario(username,passw,nombre,rango,idEncargado) VALUES ('Pedro','pedro','pedro',2,2)
INSERT INTO Usuario(username,passw,nombre,rango,idEncargado) VALUES ('Christina','christina','christina',3,3)
GO
INSERT INTO Encargado(nombre) VALUES ('Juan')
INSERT INTO Encargado(nombre) VALUES ('Pedro')
INSERT INTO Encargado(nombre) VALUES ('Christina')
GO
INSERT INTO Cliente(nombre) VALUES ('Jared')
INSERT INTO Cliente(nombre) VALUES ('Diego')
INSERT INTO Cliente(nombre) VALUES ('Abner')
GO
INSERT INTO Herramienta(nombre,cantidad) VALUES ('Soldadora', 4)
INSERT INTO Herramienta(nombre,cantidad) VALUES ('Gato Hidraulico', 9)
INSERT INTO Herramienta(nombre,cantidad) VALUES ('Cautin', 20)
GO
INSERT INTO Estado(nombre) VALUES ('Pendiente')
INSERT INTO Estado(nombre) VALUES ('Entregado')
INSERT INTO Estado(nombre) VALUES ('Fuera de Fecha')
GO
INSERT INTO Prestamo(idCliente, idEncargado, cantidad, codigoSecc, hora, fecha,idEstado) VALUES (1, 1, 2, 'BA5AM','12:45','26-05-2014',3)
INSERT INTO Prestamo(idCliente, idEncargado, cantidad, codigoSecc, hora, fecha,idEstado) VALUES (2, 2, 3, 'BA5AM','12:45','26-05-2014',3)
INSERT INTO Prestamo(idCliente, idEncargado, cantidad, codigoSecc, hora, fecha,idEstado) VALUES (3, 3, 4, 'BA5AM','12:45','30-05-2014',1)
GO
INSERT INTO DetallePrestamo(idPrestamo, idHerramienta, cantidad) VALUES (1, 1, 1)
INSERT INTO DetallePrestamo(idPrestamo, idHerramienta, cantidad) VALUES (1, 2, 1)
INSERT INTO DetallePrestamo(idPrestamo, idHerramienta, cantidad) VALUES (2, 1, 2)
INSERT INTO DetallePrestamo(idPrestamo, idHerramienta, cantidad) VALUES (2, 2, 1)
INSERT INTO DetallePrestamo(idPrestamo, idHerramienta, cantidad) VALUES (3, 3, 4)
GO

CREATE PROCEDURE ActualizarHerramientasCantidad @idHerramienta INT, @cantidadN INT
AS
BEGIN
	DECLARE @cantidad INT
	SET @cantidad=(SELECT cantidad FROM Herramienta WHERE idHerramienta=@idHerramienta)
	UPDATE Herramienta SET cantidad=@cantidadN+@cantidad WHERE idHerramienta=@idHerramienta
END
GO
--EXEC ActualizarHerramientasCantidad 1,1
--use dbbodega
--drop database dbbodegak 

SELECT idUsuario, username,passw,nombre,rango,idEncargado FROM Usuario
SELECT * FROM Usuario
SELECT * FROM Encargado
SELECT * FROM Estado
SELECT * FROM Cliente
SELECT * FROM Herramienta
SELECT * FROM Prestamo
SELECT * FROM DetallePrestamo

--Actualizar cantidad al insertar Detalle de Prestamo
GO
CREATE TRIGGER ActualizarCantidadPrestamo
ON DetallePrestamo 
AFTER INSERT
AS
BEGIN
	DECLARE @cantidad INT, @idPrestamo INT, @idHerramienta INT
	SET @cantidad= (SELECT cantidad FROM inserted)
	SET @idPrestamo= (SELECT idPrestamo FROM inserted)
	SET @idHerramienta= (SELECT idHerramienta FROM inserted)
	UPDATE Prestamo SET cantidad=cantidad+@cantidad WHERE idPrestamo=idPrestamo
	UPDATE Herramienta SET cantidad=cantidad-@cantidad WHERE idHerramienta=@idHerramienta
END
GO
CREATE PROCEDURE UltimoIdPrestamo
AS
BEGIN
	SELECT MAX(idPrestamo) AS Maximo FROM Prestamo
END
GO
SELECT * FROM Usuario
SELECT * FROM Prestamo
SELECT * FROM DetallePrestamo
GO
CREATE PROCEDURE EliminarProfesor @idCliente INT
AS
BEGIN
	DECLARE @idPrestamo INT
	DECLARE cCurosrEliminar CURSOR
		FOR SELECT idPrestamo FROM Prestamo WHERE idCliente=@idCliente
	OPEN cCurosrEliminar
	FETCH cCurosrEliminar INTO @idPrestamo
	WHILE(@@FETCH_STATUS=0)
	BEGIN
		DELETE FROM DetallePrestamo WHERE idPrestamo=@idPrestamo
		FETCH cCurosrEliminar INTO @idPrestamo	
	END
	CLOSE cCurosrEliminar
	DEALLOCATE cCurosrEliminar
	DELETE FROM Prestamo WHERE idCliente=@idCliente
	DELETE FROM Cliente WHERE idCliente=@idCliente
END
GO
CREATE PROCEDURE Eliminar @idUsuario INT,@idEncargado INT
AS
BEGIN
	DECLARE @idPrestamo INT
	DECLARE cCurosrEliminar CURSOR
		FOR SELECT idPrestamo FROM Prestamo WHERE idEncargado=@idEncargado
	OPEN cCurosrEliminar
	FETCH cCurosrEliminar INTO @idPrestamo
	WHILE(@@FETCH_STATUS=0)
	BEGIN
		DELETE FROM DetallePrestamo WHERE idPrestamo=@idPrestamo
		FETCH cCurosrEliminar INTO @idPrestamo	
	END
	CLOSE cCurosrEliminar
	DEALLOCATE cCurosrEliminar
	DELETE FROM Prestamo WHERE idEncargado=@idEncargado
	DELETE FROM Encargado WHERE idEncargado=@idEncargado
	DELETE FROM Usuario WHERE idUsuario=@idUsuario
END
GO
--eliminar prestamo incorrecto
CREATE PROCEDURE eliminarPrestamoIncorrecto
AS
BEGIN
	DECLARE @idPrestamo INT
	SET @idPrestamo=(SELECT MAX(idPrestamo) FROM Prestamo)
	DELETE FROM DetallePrestamo WHERE idPrestamo=@idPrestamo
	DELETE FROM Prestamo WHERE idPrestamo=@idPrestamo
END
GO
--Cambiar estado de prestamo
CREATE TRIGGER ActualizarEstado
ON Devolucion
AFTER INSERT
AS
BEGIN
	DECLARE @idPrestamo INT
	SET @idPrestamo=(SELECT idPrestamo FROM inserted)
	UPDATE Prestamo SET idEstado=2 WHERE idPrestamo=@idPrestamo
	
END
GO
--Actualizar cantidad al insertar Detalle de Devolucion
CREATE TRIGGER ActualizarCantidadDevolucion
ON DetalleDevolucion
AFTER INSERT 
AS
BEGIN
	DECLARE @cantidad INT, @idDevolucion INT, @idHerramienta INT
	SET @cantidad= (SELECT cantidad FROM inserted)
	SET @idDevolucion= (SELECT idDevolucion FROM inserted)
	SET @idHerramienta= (SELECT idHerramienta FROM inserted)
	UPDATE Herramienta SET cantidad=cantidad+@cantidad WHERE idHerramienta=@idHerramienta
END
GO
--Consulta para ver cantidad de herramientas en bodega
CREATE PROCEDURE ConsultarCantidadBodega
AS
BEGIN
	SELECT SUM(Herramienta.cantidad) AS HerramientasEnBodega FROM Herramienta
END
GO
--Consultar la cantidad de herramientas que tiene cada categoria
CREATE PROCEDURE ConsultarHerramientasCategoria
AS
BEGIN
	SELECT Categoria.nombre AS Categoria, SUM(Herramienta.cantidad) AS CantidadHerramientas FROM Herramienta
	INNER JOIN Categoria ON Categoria.idCategoria=Herramienta.idCategoria
	GROUP BY Categoria.nombre
END
--Consultar cantidad que hay de cada herramienta
GO
CREATE PROCEDURE ConsultarCantidadHerramientas
AS
BEGIN
	SELECT Herramienta.nombre AS Herramienta, Categoria.nombre AS Categoria, Herramienta.cantidad AS Cantidad FROM Herramienta
	INNER JOIN Categoria ON Herramienta.idCategoria=Categoria.idCategoria
END
--Consultar veces que ha sido prestadas las herramientas
GO
CREATE PROCEDURE ConsultarHerramientasPrestadas
AS
BEGIN
	SELECT Herramienta.nombre AS Herramienta, COUNT(*) FROM DetallePrestamo 
	INNER JOIN Herramienta ON Herramienta.idHerramienta=DetallePrestamo.idHerramienta
	GROUP BY Herramienta.nombre
END
--VER PRESTAMOS DE CLIENTE
GO
CREATE PROCEDURE verificarPrestamos @idCliente INT, @idPrestamo INT
AS
BEGIN
	SELECT Prestamo.idPrestamo AS idPrestamo, Cliente.idCliente AS idCliente, Encargado.idEncargado AS idEncargado, Herramienta.nombre AS Herramienta, DetallePrestamo.cantidad AS Cantidad, Cliente.nombre AS Cliente, Prestamo.fecha AS Fecha, Prestamo.hora AS Hora, DetallePrestamo.idHerramienta AS idHerramienta FROM Prestamo
		INNER JOIN Encargado ON Prestamo.idEncargado=Encargado.idEncargado
		INNER JOIN Cliente ON Prestamo.idCliente=Cliente.idCliente
		INNER JOIN DetallePrestamo ON Prestamo.idPrestamo=DetallePrestamo.idPrestamo
		INNER JOIN Herramienta ON DetallePrestamo.idHerramienta=Herramienta.idHerramienta
		WHERE Prestamo.idCliente=@idCliente AND Prestamo.idPrestamo=@idPrestamo
END
GO
CREATE PROCEDURE verificarPrestamoNoVisto @idCliente INT, @idVerificacion INT
AS
BEGIN
	SELECT Verificacion.idVerificacion AS idPrestamo, Cliente.idCliente AS idCliente, Encargado.idEncargado AS idEncargado, Herramienta.nombre AS Herramienta, DetallePrestamo.cantidad AS Cantidad, Cliente.nombre AS Cliente, Verificacion.fecha AS Fecha, DetallePrestamo.idHerramienta AS idHerramienta FROM Verificacion
		INNER JOIN Encargado ON Verificacion.idEncargado=Encargado.idEncargado
		INNER JOIN Cliente ON Verificacion.idCliente=Cliente.idCliente
		INNER JOIN DetallePrestamo ON Verificacion.idVerificacion=DetallePrestamo.idPrestamo
		INNER JOIN Herramienta ON DetallePrestamo.idHerramienta=Herramienta.idHerramienta
		WHERE Verificacion.idCliente=@idCliente AND Verificacion.idVerificacion=@idVerificacion
END

GO
CREATE PROCEDURE EliminarHerramienta @idHerramienta INT
AS
BEGIN
	DELETE FROM DetallePrestamo WHERE idHerramienta=@idHerramienta
	DELETE FROM Herramienta WHERE idHerramienta=@idHerramienta
END
SELECT * FROM Prestamo
SELECT * FROM Usuario
UPDATE Prestamo SET idEstado=2 WHERE idPrestamo=2
SELECT Cliente.nombre AS Cliente, Estado.nombre AS NombreEstado,Prestamo.idEstado AS Estado , Prestamo.idPrestamo,Prestamo.idEncargado,Cliente.idCliente,cantidad,codigoSecc,hora,fecha, Encargado.nombre AS encargado FROM Prestamo INNER JOIN Encargado ON Prestamo.idEncargado=Encargado.idEncargado INNER JOIN Estado ON Prestamo.idEstado=Estado.idEstado INNER JOIN Cliente ON Prestamo.idCliente=Cliente.idCliente