package org.jarriaza.manejadores;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import java.sql.SQLException;
import java.sql.ResultSet;

import org.jarriaza.beans.Herramienta;
import org.jarriaza.beans.Usuario;
import org.jarriaza.db.Conexion;
import org.jarriaza.manejadores.ManejadorCliente;
/**
	Manejador del beans herramienta
*/
public class ManejadorHerramienta{
	private ObservableList<Herramienta> listaHerramienta;
	private Conexion cnx;
	private Usuario usuarioConectado;
	/**
		Instancia la lista observable
		@param cnx
	*/
	public ManejadorHerramienta(Conexion cnx){
		this.listaHerramienta = FXCollections.observableArrayList();
		this.cnx = cnx;
	}
	/**
		Ingresa un usuario conectado
		@param usuarioConectado
	*/
	public void setUsuarioConectado(Usuario usuarioConectado){
		this.usuarioConectado=usuarioConectado;
		actualizarHerramientas();
	}
	/**
		Obtiene el usuario conectado
		@return usuarioConectado
	*/
	public Usuario getUsuarioConectado(){
		return this.usuarioConectado;
	}
	/**
		Actualiza la lista de herramientas
	*/
	public void actualizarHerramientas(){
		if(usuarioConectado!=null){
			ResultSet resultado = cnx.ejecutarConsulta("SELECT idHerramienta,  nombre, cantidad FROM Herramienta");
			if(resultado!=null){
				listaHerramienta.clear();
				try{
					while(resultado.next()){
						Herramienta herramienta=new Herramienta(resultado.getInt("idHerramienta"), resultado.getString("nombre"),resultado.getInt("cantidad"));
						listaHerramienta.add(herramienta);
					}
				}catch(SQLException sql){
					sql.printStackTrace();
				}
			}
		}		
	}
	/**
		Obtiene la lista de herramientas
		@return listaHerramienta
	*/
	public ObservableList<Herramienta> getListaHerramientas(){
		return listaHerramienta;
	}
	/**
		Agrega una herramienta
		@param herramienta
	*/
	public void agregarHerramienta(Herramienta herramienta){
		cnx.ejecutarSentencia("INSERT INTO Herramienta(nombre , cantidad) VALUES ('"+herramienta.getNombre()+"', "+herramienta.getCantidad()+")");
		actualizarHerramientas();		
	}
	/**
		Elimina una herramienta
		@param idHerramienta
	*/
	public void eliminarHerramienta(int idHerramienta){
		cnx.ejecutarSentencia("EXEC EliminarHerramienta "+idHerramienta);
		actualizarHerramientas();
	}
	/**
		Actualiza una herramienta
		@param campo
		@param valor
		@param idHerramienta
	*/
	public void actualizarHerramienta(String campo,String valor,int idHerramienta,int cantidad){
		if(campo.equalsIgnoreCase("cantidad"))
			cnx.ejecutarSentencia("EXEC ActualizarHerramientasCantidad "+idHerramienta+","+cantidad);
		else
			cnx.ejecutarSentencia("UPDATE Herramienta SET "+campo+"='"+valor+"'WHERE idHerramienta="+idHerramienta);
		actualizarHerramientas();
	}
	/**
		Actualiza una herramienta en ambos campos
		@param nombre el nombre de la herramienta

	*/
	public void actualizarAmbosCampos(String nombre, int cantidad, int idHerramienta){
		cnx.ejecutarSentencia("UPDATE Herramienta SET nombre='"+nombre+"', cantidad="+cantidad+" WHERE idHerramienta="+idHerramienta);
		actualizarHerramientas();
	}
}
