package org.jarriaza.utilidad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
/**
	Clase que lee el .conf
*/
public class ReadText {	
	/**
		Metodo que lee la configuracion de un .conf
		@return config
	*/
	public String[] conf() {
		File f = new File( ".conf" );
		BufferedReader entradas;
		int cont=0;
		String [] config= new String[5];
		try {
			entradas = new BufferedReader( new FileReader( f ) );
			String linea;
			while(entradas.ready()){
				if(cont<=5){
					linea = entradas.readLine();
					String [] entrada = linea.split("=");
					if(entrada.length==2){
						if(entrada[0].equalsIgnoreCase("db_server")){					
							config[0]=entrada[1].replaceAll(" ", "");
						}else if(entrada[0].equalsIgnoreCase("db_name")){
							config[1]=entrada[1].replaceAll(" ", "");
						}else if(entrada[0].equalsIgnoreCase("db_user")){
							config[2]=entrada[1].replaceAll(" ", "");
						}else if(entrada[0].equalsIgnoreCase("db_pass")){
							config[3]=entrada[1].replaceAll(" ", "");
						}else if(entrada[0].equalsIgnoreCase("db_instance")){
							config[4]=entrada[1].replaceAll(" ", "");
						}
						cont++;
					}
				}
			}
			if(config[0]!=null && config[1]!=null && config[2]!=null && config[3]!=null && config[4]!=null){
				return config;	
			}
		}catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}