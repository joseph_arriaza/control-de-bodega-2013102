package org.jarriaza.manejadores;

import java.sql.SQLException;
import java.sql.ResultSet;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import javafx.beans.property.SimpleListProperty;

import org.jarriaza.db.Conexion;
import org.jarriaza.beans.Usuario;
/**
	Manejador del beans Usuario
*/
public class ManejadorUsuario{
	private Usuario usuarioConectado;
	private Conexion conexion;
	private ObservableList<Usuario> listaDeUsuarios;
	/**
		Constructor que usa una para todas las funciones
		@param conexion
	*/
	public ManejadorUsuario(Conexion conexion){
		this.listaDeUsuarios = FXCollections.observableArrayList();
		this.conexion = conexion;
	}
	/**
		actualiza la lista de usuarios
	*/
	public void actualizarLista(){
		ResultSet resultado = conexion.ejecutarConsulta("SELECT idUsuario, username,passw,nombre,rango,idEncargado FROM Usuario");
		try{
			if(resultado!=null){	
				listaDeUsuarios.clear();
				while(resultado.next()){
					Usuario user=new Usuario(resultado.getInt("idUsuario"),resultado.getString("nombre"), resultado.getString("username"),resultado.getString("passw"),resultado.getInt("rango"),resultado.getInt("idEncargado"));
					listaDeUsuarios.add(user);
				}
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	/**
		Llenar y obtiene lista de usuarios
	*/
	public ObservableList<Usuario> getListaUsuarios(){
		actualizarLista();
		return listaDeUsuarios;
	}
	/**
		Verifica si los datos ingresados son correctos
		@param username
		@param pass
	*/
	public boolean autenticar(String username, String pass){
		if(usuarioConectado!=null)
			return true;
		ResultSet resultado = conexion.ejecutarConsulta("SELECT idUsuario, username,passw,nombre,rango,idEncargado FROM Usuario WHERE username='"+username+"' AND passw='"+pass+"'");
		try{
			if(resultado!=null){
				if(resultado.next()){
					usuarioConectado=new Usuario(resultado.getInt("idUsuario"),resultado.getString("nombre"), resultado.getString("username"),resultado.getString("passw"),resultado.getInt("rango"),resultado.getInt("idEncargado"));
				}
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
		return usuarioConectado!=null;
	}
	/**
		Cierra sesion
	*/
	public void desautenticar(){
		usuarioConectado = null;
	}
	/**
		Obtiene el usuario autenticado
		@return usuarioConectado
	*/
	public Usuario getUsuarioAutenticado(){
		return this.usuarioConectado;
	}
	/**
		Obtiene el ultimo id del usuario
		@return idUsuario ultimo id del usuario
	*/
	public int getLastId(){
		int idUsuario=0;
		for(Usuario usuario:listaDeUsuarios){
			idUsuario=usuario.getIdUsuario();
		}
		return idUsuario;
	}
	/**
		Agrega un usuario a la base de datos
		@param user usuario que se agregara
	*/
	public void agregarUsuario(Usuario user){
		int idUser=getLastId()+1;
		conexion.ejecutarSentencia("INSERT INTO Usuario(username,passw,nombre,rango,idEncargado) VALUES ('"+user.getUsername()+"','"+user.getPassw()+"','"+user.getNombre()+"',"+user.getRango()+","+idUser+")");
	}
	public void actualizarUsuario(String campo, String valor,int idUsuario, int idEncargado){
		conexion.ejecutarSentencia("UPDATE Usuario SET "+campo+"='"+valor+"' WHERE idUsuario="+idUsuario);
		if(campo.equalsIgnoreCase("nombre")){
			conexion.ejecutarSentencia("UPDATE Encargado SET nombre='"+valor+"' WHERE idEncargado="+idEncargado);
		}
		actualizarLista();
	}	
	public void eliminarUsuario(Usuario user){
		conexion.ejecutarSentencia("EXEC Eliminar "+user.getIdUsuario()+","+user.getIdEncargado());
		actualizarLista();
	}
}
