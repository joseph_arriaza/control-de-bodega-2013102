package org.jarriaza.sistema;

import org.jarriaza.ui.Principal;

import javafx.application.Application;
/**
	Clase que ejecuta todo
*/
public class App{
	/**
		Metodo main que llama al Start
		@param args
	*/
	public static void main(String args[]){
		Application.launch(Principal.class, args);
	}
}
