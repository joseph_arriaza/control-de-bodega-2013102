package org.jarriaza.utilidad;
import java.util.Calendar;
import java.util.GregorianCalendar;
/**
	Metodo que da la fecha de compra y entrega
*/
public class VerificarFecha{
	/**
		Metodo que reconoce la fecha del dia para la fecha del pedido
		@return String fecha del dia
	*/
	public static String fechaHoy(){
		Calendar fecha = new GregorianCalendar();
		int mes = fecha.get(Calendar.MONTH)+1;
		int dia = fecha.get(Calendar.DAY_OF_MONTH);
		int year = fecha.get(Calendar.YEAR);
		String fechaHoy=Integer.toString(dia)+"/"+"0"+Integer.toString(mes)+"/"+Integer.toString(year);
		return fechaHoy;
	}
	/**
		Devuelve si esa fecha ya paso o no
		@return verificar True si la fecha es correcta false si ya paso
	*/
	public boolean fechaPrestamo(String fecha){
		boolean verificar=false;
		Calendar fechaHoy = new GregorianCalendar();
		int mes = fechaHoy.get(Calendar.MONTH)+1;
		int dia = fechaHoy.get(Calendar.DAY_OF_MONTH);
		int year = fechaHoy.get(Calendar.YEAR);
		String diaMesYear[]=fecha.split("-");
		if(diaMesYear.length==3){
			try{
				int diaP=Integer.parseInt(diaMesYear[0]);
				int mesP=Integer.parseInt(diaMesYear[1]);
				int yearP=Integer.parseInt(diaMesYear[2]);
				if(diaP!=0 && mesP!=00 && yearP!=0){
					if(year<=yearP){
						if(year==yearP){
							if(mes<=mesP){
								if(mes==mesP){
									if(dia<=diaP){
										verificar=true;
										return verificar;
									}else{
										return verificar;
									}
								}else{
									verificar=true;
									return verificar;
								}
							}else{
								return verificar;
							}
						}else{
							verificar=true;
							return verificar;
						}
					}else{
						return verificar;
					}
				}else{
					return verificar;
				}
			}catch(NumberFormatException e){
				return verificar;
			}
		}
		return verificar;
	}
}