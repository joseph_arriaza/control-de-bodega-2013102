package org.jarriaza.beans;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TextField;
public class Objeto{
	private String herramienta;
	private TextField cantidad;
	private SplitMenuButton sMenu;
	public Objeto(){
		super();
	}	
	public Objeto(String herramienta,TextField cantidad,SplitMenuButton sMenu){
		setHerramienta(herramienta);
		setCantidad(cantidad);
		setSMenu(sMenu);
	}
	public void setSMenu(SplitMenuButton sMenu){
		this.sMenu=sMenu;
	}
	public SplitMenuButton getSMenu(){
		return sMenu;
	}
	public void setHerramienta(String herramienta){
		this.herramienta=herramienta;
	}
	public String getHerramienta(){
		return herramienta;
	}
	public void setCantidad(TextField cantidad){
		this.cantidad=cantidad;
	}
	public TextField getCantidad(){
		return cantidad;
	}
}