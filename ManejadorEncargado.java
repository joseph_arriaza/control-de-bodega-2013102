package org.jarriaza.manejadores;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import java.sql.SQLException;
import java.sql.ResultSet;

import org.jarriaza.beans.Encargado;
import org.jarriaza.beans.Usuario;
import org.jarriaza.db.Conexion;
/**
	Manejador del beans Encargado
*/
public class ManejadorEncargado{
	private ObservableList<Encargado> listaEncargado;
	private Conexion cnx;
	private Usuario usuarioConectado;
	/**
		Instancia la lista observable
		@param cnx
	*/
	public ManejadorEncargado(Conexion cnx){
		this.listaEncargado = FXCollections.observableArrayList();
		this.cnx = cnx;
	}
	/**
		Ingresa el usuario conectado
		@param usuarioConectado
	*/
	public void setUsuarioConectado(Usuario usuarioConectado){
		this.usuarioConectado=usuarioConectado;
		actualizarEncargado();
	}
	/**
		Obtiene el usuario conectado
		@return usuarioConectado
	*/
	public Usuario getUsuarioConectado(){
		return this.usuarioConectado;
	}
	/**
		Actualiza la lista de encargados
	*/
	public void actualizarEncargado(){
		if(usuarioConectado!=null){
			ResultSet resultado = cnx.ejecutarConsulta("SELECT idEncargado, nombre  FROM Encargado");
			if(resultado!=null){
				listaEncargado.clear();
				try{
					while(resultado.next()){
						Encargado encargado=new Encargado(resultado.getInt("idEncargado"),resultado.getString("nombre"));
						listaEncargado.add(encargado);
					}
				}catch(SQLException sql){
					sql.printStackTrace();
				}
			}
		}		
	}
	/**
		Obtiene la lista de encargados
		@return listaEncargado
	*/
	public ObservableList<Encargado> getListaEncargado(){
		return listaEncargado;
	}
	/**
		Agrega un encargado
		@param encargado
	*/
	public void agregarEncargado(Encargado encargado){
		cnx.ejecutarSentencia("INSERT INTO Encargado(nombre) VALUES ('"+encargado.getNombre()+"')");
		actualizarEncargado();
		
	}
	/*
		Elimina un encargado
		@param idEncargado
	*/
	/**
		Actualiza un Encargado
		@param campo
		@param valor
		@param idEncargado
	*/
	public void actualizaUnEcargado(String campo,String valor,int idEncargado){
		cnx.ejecutarSentencia("UPDATE DetalleDevolucion SET "+campo+"='"+valor+"'WHERE idEncargado="+idEncargado);
		actualizarEncargado();
	}	
}