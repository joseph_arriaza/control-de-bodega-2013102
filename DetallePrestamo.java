package org.jarriaza.beans;


import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
/**
	Beans DetallePrestamo
*/
public class DetallePrestamo{
	private StringProperty herramienta, hora , fecha;
	private IntegerProperty idPrestamo,idHerramienta,cantidad;
	/**
		Constructor vacio
	*/
	public DetallePrestamo(){
		this.init();
	}
		/**
		Constructor Devolucion
		@param int idHerramienta
		@param int cantidad
		@param int idPrestamo
		@param String fecha
		@param String hora
		@param String herramienta
	*/
	public DetallePrestamo(int idPrestamo , int idHerramienta, int cantidad, String fecha, String hora,String herramienta){
		this.init();
		this.setHerramienta(herramienta);
		this.setIdPrestamo(idPrestamo);	
		this.setIdHerramienta(idHerramienta);	
		this.setCantidad(cantidad);	
		this.setHora(hora);
		this.setFecha(fecha);
	}
	/**
		Inicializa las variables
	*/
	private void init(){
		this.herramienta = new SimpleStringProperty();
		this.idPrestamo = new SimpleIntegerProperty();
		this.idHerramienta = new SimpleIntegerProperty();
		this.cantidad = new SimpleIntegerProperty();
		this.hora = new SimpleStringProperty();
		this.fecha = new SimpleStringProperty();
	}
	/**
		Devuelve la herramienta
		@return String herramienta
	*/
	public String getHerramienta(){
		return this.herramienta.get();
	}
	/**
		ingresa la herramienta
		@param String herramienta
	*/
	public void setHerramienta(String herramienta){
		this.herramienta.set(herramienta);
	}
	/**
		Devuelve la herramienta observable
		@return StringProperty herramienta
	*/
	public StringProperty herramientaProperty(){
		return this.herramienta;
	}
	/**
		Devuelve el idPrestamo
		@return int idPrestamo
	*/
	public int getIdPrestamo(){
		return this.idPrestamo.get();
	}/**
		Ingresa el idPrestamo
		@param int idPrestamo
	*/
	public void setIdPrestamo(int id){
		this.idPrestamo.set(id);
	}
	/**
		Devuelve el idPrestamo observable
		@return IntegerProperty idPrestamo
	*/
	public IntegerProperty idPrestamoProperty(){
		return this.idPrestamo;
	}
	/**
		Devuelve el idHerramienta
		@return int idHerramienta
	*/
	public int getIdHerramienta(){
		return this.idHerramienta.get();
	}
	/**
		Ingresa el idHerramienta
		@param int idHerramienta
	*/
	public void setIdHerramienta(int id){
		this.idHerramienta.set(id);
	}
	/**
		Devuelve el idHerramienta observable
		@return IntegerProperty idHerramienta
	*/
	public IntegerProperty idHerramientaProperty(){
		return this.idHerramienta;
	}
	/**
		Devuelve la cantidad
		@return int cantidad
	*/
	public int getCantidad(){
		return this.cantidad.get();
	}
	/**
		Ingresa la cantidad
		@param int cantidad
	*/
	public void setCantidad(int cantidad){
		this.cantidad.set(cantidad);
	}
	/**
		Devuelve la cantidad observable
		@return IntegerProperty cantidad
	*/
	public IntegerProperty cantidadProperty(){
		return this.cantidad;
	}
	/**
		Devuelve la hora
		@return String hora
	*/
	public String getHora(){
		return this.hora.get();
	}
	/**
		Ingresa la hora
		@param String hora
	*/
	public void setHora(String hora){
		this.hora.set(hora);
	}
	/**
		Devuelve la hora observable
		@return StringProperty hora
	*/
	public StringProperty horaProperty(){
		return this.hora;
	}
	/**
		Devuelve la fecha
		@return String fecha
	*/
	public String getFecha(){
		return this.fecha.get();
	}
	/**
		Ingresa la fecha
		@param String fecha
	*/
	public void setFecha(String fecha){
		this.fecha.set(fecha);
	}
	/**
		Devuelve la fecha observable
		@return StringProperty fecha
	*/
	public StringProperty fechaProperty(){
		return this.fecha;
	}
}
