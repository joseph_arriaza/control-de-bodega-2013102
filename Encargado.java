package org.jarriaza.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
/**
	Beans de encargado
*/
public class Encargado{
	private StringProperty nombre;
	private IntegerProperty idEncargado;
	/**
		Constructor que inicializa las variables
	*/
	public Encargado(){
		this.init();
	}
	/**
		Constructor que crea Encargado y pide sus valores
		@param int idEncargado
		@param String nombre
	*/
	public Encargado(int idEncargado , String nombre){
		this.init();
		this.setIdEncargado(idEncargado);	
		this.setNombre(nombre);
	}
	/**
		Metodo que inicializa las variables
	*/
	private void init(){
		this.idEncargado = new SimpleIntegerProperty();
		this.nombre = new SimpleStringProperty();
	}
	/**
		Obtiene el nombre del encargado
		@return String nombre
	*/
	public String getNombre(){
		return this.nombre.get();
	}
	/**
		Ingresa el nombre
		@param String nombre
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	/**
		Obtiene el nombre observable
		@return StringProperty nombre
	*/
	public StringProperty nombreProperty(){
		return this.nombre;
	}
	/**
		Obtiene el id
		@return int idEncargado
	*/
	public int getIdEncargado(){
		return this.idEncargado.get();
	}
	/**
		Ingresa el id
		@param int id
	*/
	public void setIdEncargado(int id){
		this.idEncargado.set(id);
	}
	/**
		Obtiene el id observable
		@return IntegerProperty idEncargado
	*/
	public IntegerProperty idEncargadoProperty(){
		return this.idEncargado;
	}
}
